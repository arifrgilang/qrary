package com.express.data.common

enum class SourceType{
    LOCAL, NETWORK, MOCK
}

