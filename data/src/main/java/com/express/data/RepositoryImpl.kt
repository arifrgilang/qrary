package com.express.data

import com.express.data.qrary.ApiServices
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.orhanobut.hawk.Hawk
import com.express.domain.qrary.Repository
import com.express.domain.qrary.USER_TOKEN
import com.express.domain.model.account.MChangePwd
import com.express.domain.model.account.MStudentData
import com.express.domain.model.account.MUser
import com.express.domain.model.auth.*
import com.express.domain.model.book.MAddBook
import com.express.domain.model.book.MBook
import com.express.domain.model.book.MDeleteBook
import com.express.domain.model.book.MUpdateBook
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.loan.MPeminjam
import com.express.domain.model.loan.MReturnBookKey
import com.express.domain.model.ticket.*
import com.express.domain.model.visitor.MVisitor
import com.express.domain.model.visitor.MVisitorResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val apiService: ApiServices
): Repository {
    override fun login(
        loginModel: MLogin
    ): Single<ResponseObject<MTokenRole>> {
        return apiService.login(loginModel)
    }

    override fun register(
        registerModel: MRegister
    ): Single<ResponseObject<MUser>>{
        return apiService.register(registerModel)
    }

    override fun sendOTP(
        email: MEmail
    ): Single<ResponseObject<MOTPResponse>>{
        return apiService.sendOTP(email)
    }

    override fun verifyOTP(
        otpBody: MVerifyOTP
    ): Single<ResponseObject<MVerifyOTPResponse>> {
        return apiService.verifyOTP(otpBody)
    }

    override fun addVisitor(
        visitor: MVisitor
    ): Single<ResponseObject<MVisitorResponse>> {
        return apiService.addVisitor(Hawk.get(USER_TOKEN), visitor)
    }

    override fun addBook(
        partMap: HashMap<String, RequestBody>,
        coverBuku: MultipartBody.Part
    ): Single<ResponseObject<MBook>> {
        return apiService.addBook(
            Hawk.get(USER_TOKEN),
            partMap,
            coverBuku
        )
    }

    override fun getVisitor(
    ): Single<ResponseArray<MVisitorResponse>>{
        return apiService.getVisitor(Hawk.get(USER_TOKEN))
    }

    override fun getUser(
        key: String,
        value: String
    ): Single<ResponseArray<MUser>>{
        return apiService.getUser(Hawk.get(USER_TOKEN), key, value)
    }

    override fun getLibrary(
    ): Single<ResponseArray<MBook>>{
        return apiService.getLibrary(Hawk.get(USER_TOKEN))
    }

    override fun getBook(
        key: String,
        value: String
    ): Single<ResponseObject<MBook>>{
        return apiService.getBook(Hawk.get(USER_TOKEN), key, value)
    }

    override fun getListBook(
        key: String,
        value: String
    ): Single<ResponseArray<MBook>>{
        return apiService.getListBook(Hawk.get(USER_TOKEN), key, value)
    }

    override fun getTicketPeminjam(
        npm: MTempPinjam
    ): Single<ResponseObject<MTempPinjamResponse>>{
        return apiService.getTicketPeminjam(Hawk.get(USER_TOKEN), npm)
    }

    override fun getTicketDetails(
        ticketId: String
    ): Single<ResponseArray<MTicketDetails>>{
        return apiService.getTicketDetails(Hawk.get(USER_TOKEN), ticketId)
    }

    override fun addBookToTicket(
        book: MAddBookTicket
    ): Single<ResponseObject<MBook>>{
        return apiService.addBookToTicket(Hawk.get(USER_TOKEN), book)
    }

    override fun savePeminjaman(
        ticketModel: MPeminjaman
    ): Single<ResponseObject<MPeminjam>>{
        return apiService.savePeminjaman(Hawk.get(USER_TOKEN), ticketModel)
    }

    override fun getAllBorrower(
    ): Single<ResponseArray<MPeminjam>>{
        return apiService.getAllBorrower(Hawk.get(USER_TOKEN))
    }

    override fun deleteBookFromTicket(
        bookModel: MTempDelete
    ): Single<ResponseArray<MBook>>{
        return apiService.deleteBookFromTicket(Hawk.get(USER_TOKEN), bookModel)
    }

    override fun getAllHistory(
    ): Single<ResponseArray<MPeminjam>>{
        return apiService.getAllHistory(Hawk.get(USER_TOKEN))
    }

    override fun getRecentBooks(
    ): Single<ResponseArray<MBook>>{
        return apiService.getRecentBooks(Hawk.get(USER_TOKEN))
    }

    override fun getSpesificPeminjaman(
        key: String,
        id: String
    ): Single<ResponseArray<MHistoryDetail>>{
        return apiService.getSpesificPeminjaman(Hawk.get(USER_TOKEN), key, id)
    }

    override fun getSpesificHistoryPeminjaman(
        key: String,
        id: String
    ): Single<ResponseArray<MHistoryDetail>>{
        return apiService.getSpesificHistoryPeminjaman(Hawk.get(USER_TOKEN), key, id)
    }

    override fun returnBook(
        ticketId: MReturnBookKey
    ): Single<ResponseObject<MPeminjam>>{
        return apiService.returnBook(Hawk.get(USER_TOKEN), ticketId)
    }

    override fun requestSendEmail(
        email: MEmail
    ): Single<ResponseObject<MEmail>>{
        return apiService.requestSendEmail(email)
    }

    override fun deleteBook(
        bookId: String
    ): Single<ResponseObject<MDeleteBook>>{
        return apiService.deleteBook(Hawk.get(USER_TOKEN), bookId)
    }

    override fun updateBook(
        bookId: String,
        book: MUpdateBook
    ): Single<ResponseObject<MBook>>{
        return apiService.updateBook(Hawk.get(USER_TOKEN), bookId, book)
    }

    override fun changePasswordUser(
        npm: String,
        mChangePwd: MChangePwd
    ): Single<ResponseObject<MUser>> {
        return apiService.changePasswordUser(Hawk.get(USER_TOKEN), npm, mChangePwd)
    }

    override fun checkNpmAvailability(
        npm: String
    ): Single<ResponseObject<MStudentData>> {
        return apiService.checkNpmAvailability(npm)
    }

    override fun updateCover(
        bookId: String,
        file: MultipartBody.Part
    ): Single<ResponseObject<MBook>> {
        return apiService.updateCover(Hawk.get(USER_TOKEN), file, bookId)
    }
}
