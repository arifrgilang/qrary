package com.express.data.qrary

import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.express.domain.model.account.MChangePwd
import com.express.domain.model.account.MStudentData
import com.express.domain.model.account.MUser
import com.express.domain.model.auth.*
import com.express.domain.model.book.MAddBook
import com.express.domain.model.book.MBook
import com.express.domain.model.book.MDeleteBook
import com.express.domain.model.book.MUpdateBook
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.loan.MPeminjam
import com.express.domain.model.loan.MReturnBookKey
import com.express.domain.model.ticket.*
import com.express.domain.model.visitor.MVisitor
import com.express.domain.model.visitor.MVisitorResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiServices{
    // Done refactor
    @POST("auth/login")
    fun login(
        @Body loginModel: MLogin
    ): Single<ResponseObject<MTokenRole>>

    // Done refactor
    @POST("auth/register")
    fun register(
        @Body registerModel: MRegister
    ): Single<ResponseObject<MUser>>

    // Done refactor
    @POST("auth/send/OTP")
    fun sendOTP(
        @Body email: MEmail
    ): Single<ResponseObject<MOTPResponse>>

    // Done refactor
    @POST("auth/verify/OTP")
    fun verifyOTP(
        @Body otpBody: MVerifyOTP
    ): Single<ResponseObject<MVerifyOTPResponse>>

    // BATAS ACCOUNT
    // Done refactor
    @POST("pengunjung")
    fun addVisitor(
        @Header("token") token: String,
        @Body visitor: MVisitor
    ): Single<ResponseObject<MVisitorResponse>>

    // Done refactor
    @Multipart
    @POST("buku")
    fun addBook(
        @Header("token") token: String,
        @PartMap partMap: HashMap<String, RequestBody>,
        @Part coverBuku: MultipartBody.Part
    ): Single<ResponseObject<MBook>>

    // Done refactor
    @GET("pengunjung")
    fun getVisitor(
        @Header("token") token: String
    ): Single<ResponseArray<MVisitorResponse>>

    // Done refactor
    @GET("user/find")
    fun getUser(
        @Header("token") token: String,
        @Query("key") key: String,
        @Query("value") value: String
    ): Single<ResponseArray<MUser>>

    // Done refactor
    @GET("buku")
    fun getLibrary(
        @Header("token") token: String
    ): Single<ResponseArray<MBook>>

    // Done refactor
    @GET("buku/find")
    fun getBook(
        @Header("token") token: String,
        @Query("key") key: String,
        @Query("value") value: String
    ): Single<ResponseObject<MBook>>

    // Done refactor
    @GET("buku/find")
    fun getListBook(
        @Header("token") token: String,
        @Query("key") key: String,
        @Query("value") value: String
    ): Single<ResponseArray<MBook>>

    // Done refactor
    @POST("peminjaman/temp/pengunjung")
    fun getTicketPeminjam(
        @Header("token") token: String,
        @Body npm: MTempPinjam
    ): Single<ResponseObject<MTempPinjamResponse>>

    // Done refactor
    @GET("peminjaman/temp/{ticketId}")
    fun getTicketDetails(
        @Header("token") token: String,
        @Path("ticketId") ticketId: String
    ): Single<ResponseArray<MTicketDetails>>

    // Done refactor
    @POST("peminjaman/temp/buku")
    fun addBookToTicket(
        @Header("token") token: String,
        @Body book: MAddBookTicket
    ): Single<ResponseObject<MBook>>

    // Done refactor
    @POST("peminjaman")
    fun savePeminjaman(
        @Header("token") token: String,
        @Body ticketModel: MPeminjaman
    ): Single<ResponseObject<MPeminjam>>

    // Done refactor
    @GET("peminjaman")
    fun getAllBorrower(
        @Header("token") token: String
    ): Single<ResponseArray<MPeminjam>>

    // Done refactor
    @DELETE("peminjaman/temp/buku")
    fun deleteBookFromTicket(
        @Header("token") token: String,
        @Body bookModel: MTempDelete
    ): Single<ResponseArray<MBook>>

    // Done refactor
    @GET("historypeminjaman")
    fun getAllHistory(
        @Header("token") token: String
    ): Single<ResponseArray<MPeminjam>>

    // Done refactor
    @GET("buku/recent?limit=6")
    fun getRecentBooks(
        @Header("token") token: String
    ): Single<ResponseArray<MBook>>

    // Done refactor
    @GET("peminjaman/find")
    fun getSpesificPeminjaman(
        @Header("token") token: String,
        @Query("key") key: String,
        @Query("value") value: String
    ): Single<ResponseArray<MHistoryDetail>>

    // Done refactor
    @GET("historypeminjaman/find")
    fun getSpesificHistoryPeminjaman(
        @Header("token") token: String,
        @Query("key") key: String,
        @Query("value") value: String
    ): Single<ResponseArray<MHistoryDetail>>

    // Done refactor
    @PUT("peminjaman")
    fun returnBook(
        @Header("token") token: String,
        @Body ticketId: MReturnBookKey
    ): Single<ResponseObject<MPeminjam>>

    // CHANGE LATER ON
    @POST("user/password/sendmail")
    fun requestSendEmail(
        @Body email: MEmail
    ): Single<ResponseObject<MEmail>>

    // Done refactor
    @DELETE("buku/{bookId}")
    fun deleteBook(
        @Header("token") token: String,
        @Path("bookId") bookId: String
    ): Single<ResponseObject<MDeleteBook>>

    // Done refactor
    @PUT("buku/{bookId}")
    fun updateBook(
        @Header("token") token: String,
        @Path("bookId") bookId: String,
        @Body book: MUpdateBook
    ): Single<ResponseObject<MBook>>

    // Done refactor
    @PUT("user/password/{npm}")
    fun changePasswordUser(
        @Header("token") token: String,
        @Path("npm") npm: String,
        @Body mChangePwd: MChangePwd
    ): Single<ResponseObject<MUser>>

    @GET("auth/npmdata/{npm}")
    fun checkNpmAvailability(
        @Path("npm") npm: String
    ): Single<ResponseObject<MStudentData>>

    @Multipart
    @PUT("buku/{bookId}/cover")
    fun updateCover(
        @Header("token") token: String,
        @Part file: MultipartBody.Part,
        @Path("bookId") bookId: String
    ) : Single<ResponseObject<MBook>>
}