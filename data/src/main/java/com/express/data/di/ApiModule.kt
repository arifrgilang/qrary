package com.express.data.di

import com.express.data.RepositoryImpl
import com.express.data.qrary.ApiServices
import com.express.domain.qrary.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ApiModule {

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit.Builder): ApiServices =
        retrofit
            .build()
            .create(ApiServices::class.java)

    @Provides
    @Singleton
    fun provideDataManager(repository: RepositoryImpl): Repository = repository
}