import org.gradle.kotlin.dsl.DependencyHandlerScope

object DefaultConfig {
    val appId = "com.rz.qrary"

    val minSdk = 21
    val targetSdk = 29
    val compileSdk = 29
    val buildTools = "29.0.3"

    val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}

object Releases {
    val versionCode = 4
    val versionName = "2.1"
}

object Versions {
    val kotlin = "1.3.72"
    val appCompat = "1.1.0"
    val coreKtx = "1.3.0"
    val constraintLayout = "1.1.3"
    val lifecycle = "1.1.1"
    val anko = "0.10.8"
    val preference = "1.1.1"
    val activityKtx = "1.1.0"
    val fragmentKtx = "1.2.5"

    val material = "1.1.0"
    val swipeRefreshLayout = "1.1.0-rc01"
    val circularImage = "4.0.2"
    val spinnerDatePicker = "2.0.1"
    val discreteScroll = "1.4.9"
    val vpDots = "4.1.2"
    val scrollIndicator = "1.2.1"
    val blurImage = "1.0.1"
    val otpView = "v1.1.2-ktx"

    val qrModule = "1.0"
    val hawk = "2.0.1"
    val dexter = "6.2.2"

    val glide = "4.10.0"
    val glideLegacy = "1.0.0"
    val glideKapt = "4.10.0"

    val retrofit = "2.7.1"
    val okHttp = "3.12.12"

    val rxJava = "2.2.10"
    val rxAndroid = "2.1.1"

    val hilt = "2.28-alpha"
    val hiltViewModel = "1.0.0-alpha01"
    val hiltCompiler = "1.0.0-alpha01"

    val junit = "4.13"
    val extJunit = "1.1.1"
    val espresso = "3.2.0"
}



fun DependencyHandlerScope.commonDependencies() {
    "implementation"(Deps.appCompat)
    "implementation"(Deps.kotlin)
    "implementation"(Deps.constraintLayout)
    "testImplementation"(Deps.junit)
    "androidTestImplementation"(Deps.extJunit)
    "androidTestImplementation"(Deps.espresso)
}

fun DependencyHandlerScope.appDependencies(){

    // Support Library
    "implementation"(Deps.kotlin)
    "implementation"(Deps.appCompat)
    "implementation"(Deps.coreKtx)
    "implementation"(Deps.constraintLayout)
    "implementation"(Deps.lifeCycle)
    "implementation"(Deps.anko)
    "implementation"(Deps.preference)
    "implementation"(Deps.activityKtx)
    "implementation"(Deps.fragmentKtx)

    // UI Material
    "implementation"(Deps.material)
    "implementation"(Deps.swipeRefreshLayout)
    "implementation"(Deps.circularImage)
    "implementation"(Deps.spinnerDatePicker)
    "implementation"(Deps.discreteScroll)
    "implementation"(Deps.vpDots)
    "implementation"(Deps.scrollIndicator)
    "implementation"(Deps.blurImage)
    "implementation"(Deps.otpView)

    // Glide
    "implementation"(Deps.glide)
    "implementation"(Deps.glideLegacy)
    "kapt"(Deps.glideKapt)


    // Retrofit
    "implementation"(Deps.retrofit)
    "implementation"(Deps.retrofitRxJava)
    "implementation"(Deps.retrofitGson)

    // OkHttp
    "implementation"(Deps.okHttp)
    "implementation"(Deps.interceptor)

    // RxJava Library
    "implementation"(Deps.rxJava)
    "implementation"(Deps.rxAndroid)

    // Dagger Hilt
    "implementation"(Deps.hilt)
    "implementation"(Deps.hiltViewModel)
    "kapt"(Deps.hiltAndroidCompiler)
    "kapt"(Deps.hiltCompiler)

    // Test
    "testImplementation"(Deps.junit)
    "androidTestImplementation"(Deps.extJunit)
    "androidTestImplementation"(Deps.espresso)
}

fun DependencyHandlerScope.domainDependencies(){

}

object Deps {
    // Support Library
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    val lifeCycle = "android.arch.lifecycle:extensions:${Versions.lifecycle}"
    val anko = "org.jetbrains.anko:anko:${Versions.anko}"
    val preference = "androidx.preference:preference:${Versions.preference}"
    val activityKtx = "androidx.activity:activity-ktx:${Versions.activityKtx}"
    val fragmentKtx = "androidx.fragment:fragment-ktx:${Versions.fragmentKtx}"

    // Material
    val material = "com.google.android.material:material:${Versions.material}"
    val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefreshLayout}"
    val circularImage = "com.mikhaellopez:circularimageview:${Versions.circularImage}"
    val spinnerDatePicker = "com.github.drawers:SpinnerDatePicker:${Versions.spinnerDatePicker}"
    val discreteScroll = "com.yarolegovich:discrete-scrollview:${Versions.discreteScroll}"
    val vpDots = "com.tbuonomo.andrui:viewpagerdotsindicator:${Versions.vpDots}"
    val scrollIndicator = "ru.tinkoff.scrollingpagerindicator:scrollingpagerindicator:${Versions.scrollIndicator}"
    val blurImage = "com.github.jgabrielfreitas:BlurImageView:${Versions.blurImage}"
    val otpView = "com.github.aabhasr1:OtpView:${Versions.otpView}"

    // Core Feature Module
    val qrModule = "me.ydcool.lib:qrmodule:${Versions.qrModule}"
    val hawk = "com.orhanobut:hawk:${Versions.hawk}"
    val dexter = "com.karumi:dexter:${Versions.dexter}"

    // Glide
    val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    val glideLegacy = "androidx.legacy:legacy-support-v4:${Versions.glideLegacy}"
    val glideKapt = "com.github.bumptech.glide:compiler:${Versions.glideKapt}"

    // Retrofit
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitRxJava = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    val retrofitGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"

    // OkHttp
    val interceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okHttp}"

    // RxJava
    val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"

    // Dagger Hilt
    val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
    val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
    val hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltViewModel}"
    val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltCompiler}"

    // Test
    val junit = "junit:junit:${Versions.junit}"
    val extJunit = "androidx.test.ext:junit:${Versions.extJunit}"
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}