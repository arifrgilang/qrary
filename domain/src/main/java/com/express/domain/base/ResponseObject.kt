package com.express.domain.base

class ResponseObject<Model> : BaseResponse() {
    var data: Model ?= null
    var total: Int? = null
}