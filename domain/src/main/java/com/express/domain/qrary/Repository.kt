package com.express.domain.qrary

import com.express.domain.base.*
import com.express.domain.model.account.MChangePwd
import com.express.domain.model.account.MStudentData
import com.express.domain.model.account.MUser
import com.express.domain.model.auth.*
import com.express.domain.model.book.MAddBook
import com.express.domain.model.book.MBook
import com.express.domain.model.book.MDeleteBook
import com.express.domain.model.book.MUpdateBook
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.loan.MPeminjam
import com.express.domain.model.loan.MReturnBookKey
import com.express.domain.model.ticket.*
import com.express.domain.model.visitor.MVisitor
import com.express.domain.model.visitor.MVisitorResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Header
import retrofit2.http.Part
import retrofit2.http.Path

interface Repository {
    fun login(
        loginModel: MLogin
    ): Single<ResponseObject<MTokenRole>>

    fun register(
        registerModel: MRegister
    ): Single<ResponseObject<MUser>>

    fun sendOTP(
        email: MEmail
    ): Single<ResponseObject<MOTPResponse>>

    fun verifyOTP(
        otpBody: MVerifyOTP
    ): Single<ResponseObject<MVerifyOTPResponse>>

    fun addVisitor(
        visitor: MVisitor
    ): Single<ResponseObject<MVisitorResponse>>

    fun addBook(
        partMap: HashMap<String, RequestBody>,
        coverBuku: MultipartBody.Part
    ): Single<ResponseObject<MBook>>

    fun getVisitor(
    ): Single<ResponseArray<MVisitorResponse>>

    fun getUser(
        key: String,
        value: String
    ): Single<ResponseArray<MUser>>

    fun getLibrary(
    ): Single<ResponseArray<MBook>>

    fun getBook(
        key: String,
        value: String
    ): Single<ResponseObject<MBook>>

    fun getListBook(
        key: String,
        value: String
    ): Single<ResponseArray<MBook>>

    fun getTicketPeminjam(
        npm: MTempPinjam
    ): Single<ResponseObject<MTempPinjamResponse>>

    fun getTicketDetails(
        ticketId: String
    ): Single<ResponseArray<MTicketDetails>>

    fun addBookToTicket(
        book: MAddBookTicket
    ): Single<ResponseObject<MBook>>

    fun savePeminjaman(
        ticketModel: MPeminjaman
    ): Single<ResponseObject<MPeminjam>>

    fun getAllBorrower(
    ): Single<ResponseArray<MPeminjam>>

    fun deleteBookFromTicket(
        bookModel: MTempDelete
    ): Single<ResponseArray<MBook>>

    fun getAllHistory(
    ): Single<ResponseArray<MPeminjam>>

    fun getRecentBooks(
    ): Single<ResponseArray<MBook>>

    fun getSpesificPeminjaman(
        key: String,
        id: String
    ): Single<ResponseArray<MHistoryDetail>>

    fun getSpesificHistoryPeminjaman(
        key: String,
        id: String
    ): Single<ResponseArray<MHistoryDetail>>

    fun returnBook(
        ticketId: MReturnBookKey
    ): Single<ResponseObject<MPeminjam>>

    fun requestSendEmail(
        email: MEmail
    ): Single<ResponseObject<MEmail>>

    fun deleteBook(
        bookId: String
    ): Single<ResponseObject<MDeleteBook>>

    fun updateBook(
        bookId: String,
        book: MUpdateBook
    ): Single<ResponseObject<MBook>>

    fun changePasswordUser(
        npm: String,
        mChangePwd: MChangePwd
    ): Single<ResponseObject<MUser>>

    fun checkNpmAvailability(
        npm: String
    ): Single<ResponseObject<MStudentData>>

    fun updateCover(
        bookId: String,
        file: MultipartBody.Part
    ) : Single<ResponseObject<MBook>>
}