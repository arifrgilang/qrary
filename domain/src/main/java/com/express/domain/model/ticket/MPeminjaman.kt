package com.express.domain.model.ticket

import com.google.gson.annotations.SerializedName

data class MPeminjaman(
    @SerializedName("idTempPinjam")
    val id: String?,
    @SerializedName("tglMeminjam")
    val tglMeminjam: String?,
    @SerializedName("tglKembali")
    val tglKembali: String
)