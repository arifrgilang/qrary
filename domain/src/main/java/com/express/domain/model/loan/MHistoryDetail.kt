package com.express.domain.model.loan

import com.express.domain.model.account.MUser
import com.express.domain.model.book.MBook
import com.google.gson.annotations.SerializedName

data class MHistoryDetail(
    @SerializedName("_id")
    val ticketId: String?,
    @SerializedName("isDikembalikan")
    val isDikembalikan: Boolean?,
    @SerializedName("tanggalMeminjam")
    val tanggalMeminjam: String?,
    @SerializedName("tanggalKembali")
    val tanggalKembali: String?,
    @SerializedName("user")
    val user: MUser?,
    @SerializedName("buku")
    val books: List<MBook>?
)