package com.express.domain.model.auth

import com.google.gson.annotations.SerializedName

data class MVerifyOTP(
    @SerializedName("otp")
    val otp: String?,
    @SerializedName("email")
    val email: String?
)