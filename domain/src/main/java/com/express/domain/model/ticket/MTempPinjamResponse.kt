package com.express.domain.model.ticket

import com.express.domain.model.account.MUser
import com.google.gson.annotations.SerializedName

data class MTempPinjamResponse(
    @SerializedName("_id")
    val id: String?,
    @SerializedName("pengunjung")
    val pengunjung: MUser?
)