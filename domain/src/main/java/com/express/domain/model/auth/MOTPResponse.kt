package com.express.domain.model.auth

import com.google.gson.annotations.SerializedName

data class MOTPResponse(
    @SerializedName("accepted")
    val accepted: ArrayList<String>?,
    @SerializedName("rejected")
    val rejected: ArrayList<String>?,
    @SerializedName("response")
    val response: String?,
    @SerializedName("messageId")
    val messageId: String?
)