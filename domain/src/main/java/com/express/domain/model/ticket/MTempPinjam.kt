package com.express.domain.model.ticket

import com.google.gson.annotations.SerializedName

data class MTempPinjam(
    @SerializedName("npm")
    val npm: String?
)