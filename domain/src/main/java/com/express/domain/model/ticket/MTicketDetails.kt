package com.express.domain.model.ticket

import com.express.domain.model.book.MBook
import com.google.gson.annotations.SerializedName

data class MTicketDetails(
    @SerializedName("_id")
    val id: String?,
    @SerializedName("idUser")
    val idUser: String?,
    @SerializedName("buku")
    val listBuku: List<MBook>?
)