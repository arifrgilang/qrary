package com.express.domain.model.auth

import java.io.Serializable

data class MLogin(
    val npm: String?,
    val pwd: String?
): Serializable