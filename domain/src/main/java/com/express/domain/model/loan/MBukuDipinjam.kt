package com.express.domain.model.loan

import com.express.domain.model.book.MBook

data class MBukuDipinjam(
    val buku: MBook?,
    val tanggalPinjam: String?,
    val tanggalKembali: String?,
    val status: String?
)