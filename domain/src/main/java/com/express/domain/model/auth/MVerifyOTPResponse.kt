package com.express.domain.model.auth

import com.google.gson.annotations.SerializedName

data class MVerifyOTPResponse(
    @SerializedName("n")
    val n: Int?,
    @SerializedName("ok")
    val ok: Int?
)