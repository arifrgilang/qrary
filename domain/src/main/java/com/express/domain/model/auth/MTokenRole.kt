package com.express.domain.model.auth

import com.google.gson.annotations.SerializedName

data class MTokenRole(
    @SerializedName("token")
    val token: String?,
    @SerializedName("role")
    val role: String?
)