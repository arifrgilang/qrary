package com.express.domain.model.book

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody

data class MAddBook (
    @SerializedName("kategori")
    val kategori: String?,
    @SerializedName("bahasa")
    val bahasa: String?,
    @SerializedName("isbn")
    val isbn: String?,
    @SerializedName("jmlHal")
    val jmlHal: Int?,
    @SerializedName("judul")
    val judul: String?,
    @SerializedName("deskripsi")
    val deskripsi: String?,
    @SerializedName("penerbit")
    val penerbit: String?,
    @SerializedName("penulis")
    val penulis: String?,
    @SerializedName("penerjemah")
    val penerjemah: String?,
    @SerializedName("tanggalTerbit")
    val tanggalTerbit: String?,
    @SerializedName("urlFoto")
    val urlFoto: String?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("coverBuku")
    val coverBuku: MultipartBody.Part?
)
