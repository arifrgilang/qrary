package com.express.domain.model.book

import com.google.gson.annotations.SerializedName

data class MDeleteBook (
    @SerializedName("ok")
    val ok: String?
)