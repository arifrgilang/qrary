package com.express.domain.model.auth

import java.io.Serializable

data class MRegister (
    val nama: String?,
    val npm: String?,
    val email: String?,
    val pwd: String?,
    val confirmPwd: String?
) : Serializable


