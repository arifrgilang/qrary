package com.express.domain.model.ticket

import com.google.gson.annotations.SerializedName

data class MTempDelete(
    @SerializedName("isbn")
    val isbn: String?,
    @SerializedName("idTempPinjam")
    val idTableTemp: String?
)
