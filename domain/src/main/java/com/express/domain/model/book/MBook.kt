package com.express.domain.model.book

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MBook (
    @SerializedName("_id")
    val id: String?,
    @SerializedName("kategori")
    val kategori: List<String>?,
    @SerializedName("bahasa")
    val bahasa: String?,
    @SerializedName("isbn")
    val isbn: String?,
    @SerializedName("jmlHal")
    val jmlHal: Int?,
    @SerializedName("judul")
    val judul: String?,
    @SerializedName("deskripsi")
    val deskripsi: String?,
    @SerializedName("penerbit")
    val penerbit: String?,
    @SerializedName("penulis")
    val penulis: String?,
    @SerializedName("penerjemah")
    val penerjemah: String?,
    @SerializedName("tanggalTerbit")
    val tanggalTerbit: String?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("coverBuku")
    val urlCover: String?
): Serializable
