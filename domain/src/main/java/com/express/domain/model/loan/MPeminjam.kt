package com.express.domain.model.loan

import com.express.domain.model.account.MUser
import com.google.gson.annotations.SerializedName

data class MPeminjam(
    @SerializedName("isbnBuku")
    val isbnBuku: List<String>?,
    @SerializedName("isDikembalikan")
    val isDikembalikan: Boolean?,
    @SerializedName("_id")
    val idTicket: String?,
    @SerializedName("idUser")
    val idUser: String?,
    @SerializedName("tanggalMeminjam")
    val tanggalMeminjam: String?,
    @SerializedName("tanggalKembali")
    val tanggalKembali: String?,
    @SerializedName("user")
    val user: MUser?
)