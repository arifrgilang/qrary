package com.express.domain.model.account

import com.google.gson.annotations.SerializedName

data class MStudentData(
    @SerializedName("Nama")
    val name: String?,
    @SerializedName("NPM")
    val npm: String?,
    @SerializedName("ProdiKode")
    val majorCode: String?,
    @SerializedName("ProdiNama")
    val majorName: String?,
    @SerializedName("Fakultas")
    val faculty: String?
)
