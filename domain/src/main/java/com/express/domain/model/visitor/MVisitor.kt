package com.express.domain.model.visitor

import com.google.gson.annotations.SerializedName

data class MVisitor(
    @SerializedName("npm")
    val npm: String?,
    @SerializedName("waktuMasuk")
    val waktuMasuk: String?
)