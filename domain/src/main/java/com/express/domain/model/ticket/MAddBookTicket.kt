package com.express.domain.model.ticket

import com.google.gson.annotations.SerializedName

data class MAddBookTicket(
    @SerializedName("isbn")
    val isbn: String?,
    @SerializedName("idTempPinjam")
    val ticketId: String?
)