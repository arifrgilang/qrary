package com.express.domain.model.account

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MUser (
    @SerializedName("_id")
    val id: String?,
    @SerializedName("nama")
    val nama: String?,
    @SerializedName("npm")
    val npm: String?,
    @SerializedName("programStudi")
    val prodi: String?,
    @SerializedName("fakultas")
    val fakultas: String?,
    @SerializedName("angkatan")
    val angkatan: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("isConfirmed")
    val isConfirmed: Boolean = false,
    @SerializedName("urlFoto")
    val urlFoto: String?,
    @SerializedName("role")
    val role: String?,
    @SerializedName("isModePinjam")
    val isModePinjam: Boolean = false
): Serializable
