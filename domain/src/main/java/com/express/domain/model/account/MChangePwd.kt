package com.express.domain.model.account

import com.google.gson.annotations.SerializedName

data class MChangePwd(
    @SerializedName("oldPwd")
    val oldPwd: String?,
    @SerializedName("newPwd")
    val newPwd: String?,
    @SerializedName("confirmNewPwd")
    val confirmNewPwd: String?
)