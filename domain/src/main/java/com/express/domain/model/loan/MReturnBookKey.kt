package com.express.domain.model.loan

import com.google.gson.annotations.SerializedName

data class MReturnBookKey(
    @SerializedName("idPeminjaman")
    val ticketId: String
)