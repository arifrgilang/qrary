package com.express.domain.model.visitor

import com.express.domain.model.account.MUser
import com.google.gson.annotations.SerializedName

data class MVisitorResponse(
    @SerializedName("_id")
    val id: String?,
    @SerializedName("user")
    val user: MUser? = null,
    @SerializedName("waktuMasuk")
    val waktuMasuk: String?
)