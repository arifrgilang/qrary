package com.express.domain.model.auth

import com.google.gson.annotations.SerializedName

data class MEmail(
    @SerializedName("email")
    val email: String?
)