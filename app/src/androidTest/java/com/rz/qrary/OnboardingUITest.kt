package com.rz.qrary

import android.content.ComponentName
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.rz.qrary.presentation.login.LoginActivity
import com.rz.qrary.presentation.onboarding.OnboardingActivity
import com.rz.qrary.presentation.register.RegisterActivity
import com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickBack
import com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class OnboardingUITest {

    @get:Rule
    var onboardingActivity: ActivityTestRule<OnboardingActivity>
        = ActivityTestRule(OnboardingActivity::class.java)

    @Test
    fun registerButton_click_successNavigate(){
        clickOn(R.id.btnOnboardingDaftar)
        clickBack()
    }

    @Test
    fun loginButton_click_successNavigate(){
        clickOn(R.id.tvOnboardingMasuk)
        clickBack()
    }
}