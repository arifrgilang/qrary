package com.rz.qrary.util.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.custom_bsd.view.*

class CustomBSD(
    private val imageRes: Int, private val content: String
) : BottomSheetDialogFragment() {

    companion object {
        @JvmStatic
        fun newInstance(imageRes: Int, content: String) =
            CustomBSD(imageRes, content)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.custom_bsd, container, false)
        view.imgBSD.setImageResource(imageRes)
        view.tvBSD.text = content
        return view
    }
}