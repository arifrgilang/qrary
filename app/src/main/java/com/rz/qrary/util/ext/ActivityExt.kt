package com.rz.qrary.util.ext

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.graphics.scale
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.rz.qrary.util.other.SingleLiveEvent
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.internals.AnkoInternals.createIntent
import org.jetbrains.anko.notificationManager
import java.io.*

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, action: (t: T) -> Unit) {
    liveData.observe(this, Observer { it?.let { t -> action(t) } })
}

fun <T> LifecycleOwner.observe(liveData: MutableLiveData<T>, action: (t: T) -> Unit) {
    liveData.observe(this, Observer { it?.let { t -> action(t) } })
}

fun <T> LifecycleOwner.observe(liveData: SingleLiveEvent<T>, action: (t: T) -> Unit) {
    liveData.observe(this, Observer { it?.let { t -> action(t) } })
}

fun Context.isConnected(): Boolean {
    val info = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
    return info != null && info.isConnected
}

@RequiresApi(Build.VERSION_CODES.O)
@JvmOverloads
fun Context.createNotificationChannel(
    id: String? = null,
    name: String? = null,
    description: String? = null,
    importance: Int = NotificationManager.IMPORTANCE_HIGH
): String? {
    if (Build.VERSION.SDK_INT < 26) return null

    val newId = id ?: packageName
    val appName = if (applicationInfo.labelRes != 0) getString(applicationInfo.labelRes) else applicationInfo.nonLocalizedLabel.toString()
    val newName = name ?: appName
    val newDescription = description ?: appName

    val mChannel = NotificationChannel(newId, newName, importance)
    mChannel.description = newDescription
    notificationManager.createNotificationChannel(mChannel)

    return newId
}

/**
 * Example:
 * ```
 * start<MainActivity> {
 *    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
 * }
 * ```
 */
inline fun <reified T : Activity> Context.start(
    vararg params: Pair<String, Any?>,
    configIntent: Intent.() -> Unit = {}
) {
    startActivity(createIntent(this, T::class.java, params).apply(configIntent))
}

/**
 * Example:
 * ```
 * start<MainActivity> {
 *    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
 * }
 * ```
 */
inline fun <reified T : Activity> Fragment.start(
    vararg params: Pair<String, Any?>,
    configIntent: Intent.() -> Unit = {}
) {
    context?.let { startActivity(createIntent(it, T::class.java, params).apply(configIntent)) }
}

/**
 * Example:
 * ```
 * startActivity(Settings.ACTION_APPLICATION_DETAILS_SETTINGS) {
 *     data = Uri.fromParts("package", packageName, null)
 * }
 * ```
 */
inline fun Context.startActivity(action: String, configIntent: Intent.() -> Unit = {}) {
    startActivity(Intent(action).apply(configIntent))
}

@SuppressLint("Recycle")
fun Context.getRealPathFromURI(
    contentUri: Uri?
): String? {
    var result = ""

    val fileId = DocumentsContract.getDocumentId(contentUri)
    val id = fileId.split(":")[1]
    val column = arrayOf(MediaStore.Images.Media.DATA)
    val selector = MediaStore.Images.Media._ID + "=?"
    val cursor = contentResolver.query(
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        column, selector, arrayOf(id), null
    )
    cursor?.getColumnIndex(column[0])?.let {
        if(cursor.moveToFirst()) {
            result = cursor.getString(it)
        }
    }
    cursor?.close()
    return result
}

fun Context.getRotatedImage(photoPath: String) : Pair<String, Bitmap> {
    val orientation = ExifInterface(photoPath)
        .getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

    val bitmap: Bitmap = BitmapFactory.decodeFile(photoPath)

    return when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90, this)
        ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180, this)
        ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270, this)
        ExifInterface.ORIENTATION_NORMAL -> rotateImage(bitmap, 0, this)
        else -> rotateImage(bitmap, 0, this)
    }
}

fun rotateImage(source: Bitmap, angle: Int, context: Context) : Pair<String, Bitmap> {
    var outWidth = 0
    var outHeight = 0
    val maxSize = 768

    val matrix = Matrix().apply {
        postRotate(angle * 1f)
    }
    var bitmap = Bitmap
        .createBitmap(
            source,
            0,
            0,
            source.width,
            source.height,
            matrix,
            true
        )

    if (bitmap.width > bitmap.height) {
        // landscape
        outWidth = maxSize
        outHeight = bitmap.height * outWidth / bitmap.width
    } else {
        // portrait
        outHeight = maxSize
        outWidth = bitmap.width * outHeight / bitmap.height
    }

    val out = ByteArrayOutputStream()

    bitmap = bitmap.scale(
        outWidth,
        outHeight,
        true
    )
    bitmap.compress(
        Bitmap.CompressFormat.JPEG,
        100,
        out
    )
    bitmap = BitmapFactory.decodeStream(
        ByteArrayInputStream(
            out.toByteArray()
        )
    )

    val file = File(
        context.cacheDir,
        "cachePhoto.jpg"
    ).apply {
        createNewFile()
    }

    //write the bytes in file
    FileOutputStream(file)
        .apply {
            write(out.toByteArray())
            flush()
            close()
        }

    return Pair(file.path, bitmap)
}

fun createMultipart(context: Context, bitmap: Bitmap) : MultipartBody.Part {
    // create temporary file
    val file = File(context.cacheDir, "coverBuku")
    file.createNewFile()

    val bos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos)
    val bitmapData = bos.toByteArray()

    val fos = FileOutputStream(file)
    fos.write(bitmapData)
    fos.flush()
    fos.close()
    // convert into multipart
    val mediaType = "multipart/form-data"
//    val mediaType = "image/*"
    val requestFile = RequestBody.create(MediaType.parse(mediaType), file)
    return MultipartBody.Part.createFormData("coverBuku", file.name, requestFile)
}

fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
    val bytes = ByteArrayOutputStream()
    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = MediaStore.Images.Media.insertImage(
        inContext.contentResolver,
        inImage,
        "Title",
        null
    )
    return Uri.parse(path)
}