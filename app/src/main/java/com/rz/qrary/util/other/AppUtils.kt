package com.rz.qrary.util.other

import android.widget.EditText
import androidx.fragment.app.FragmentManager
import com.rz.qrary.util.view.CustomBSD

class AppUtils {
    companion object{
        fun showBSD(imageRes: Int, content: String, manager: FragmentManager){
            val fragment = CustomBSD.newInstance(imageRes, content)
            fragment.show(manager, fragment.tag)
        }

        fun isEmailValid(email: String): Boolean {
            val emailPattern = "[a-zA-Z0-9._-]+@mail.unpad.ac.id"
            return email.matches(emailPattern.toRegex())
        }

        fun showErrorIfEmpty(form: EditText, message: String){
            if(form.text.toString().isEmpty()) form.error = message
        }

        fun isPasswordValid(pwd: String): Boolean {
            val passwordPatter = "((?=.*[a-z])(?=.*\\d).{6,20})"
            return pwd.matches(passwordPatter.toRegex())
        }
    }
}