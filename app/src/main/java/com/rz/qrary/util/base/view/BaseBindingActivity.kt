package com.rz.qrary.util.base.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.rz.qrary.R
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.visible
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.sdk27.coroutines.onClick

abstract class BaseBindingActivity<T : ViewDataBinding> : AppCompatActivity(){

    @LayoutRes
    protected abstract fun contentView(): Int
    protected abstract fun setupData(savedInstanceState: Bundle?)
    protected abstract fun setupView()
    protected lateinit var binding: T
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, contentView())
        setupData(savedInstanceState)
        setupView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun setupToolbar(isHasBackButton: Boolean) {
        setSupportActionBar(toolbar)
        if(isHasBackButton){
            toolbar_back.visible()
            toolbar_back.onClick {
                finish()
            }
        } else {
            toolbar_back.gone()
        }
    }

    protected fun setupToolbar(title: String, isHasBackButton: Boolean) {
        setupToolbar(isHasBackButton)
        toolbar_title.text = title
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("RESREQ", "REQ :$requestCode, RES :$resultCode")
    }
}