package com.rz.qrary.util.other

import android.text.format.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/*
    Values tested :
   "2007-05-01T15:43:26+07:00"
   "2007-05-01T15:43:26.3+07:00"
   "2007-05-01T15:43:26.3452+07:00"
   "2007-05-01T15:43:26-07:00"
   "2007-05-01T15:43:26.3-07:00"
   "2007-05-01T15:43:26.3452-07:00"
   "2007-05-01T15:43:26.3452Z"
   "2007-05-01T15:43:26.3Z"
   "2007-05-01T15:43:26Z"
*/

@Synchronized
@Throws(ParseException::class, IndexOutOfBoundsException::class)
fun parseRFC3339Date(dateString: String): Date? {
    var dateString = dateString
    var d: Date

    //if there is no time zone, we don't need to do any special parsing.
    if (dateString.endsWith("Z")) {
        try {
            val s = SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss'Z'",
                Locale.getDefault()
            ) //spec for RFC3339 with a 'Z'
            s.timeZone = TimeZone.getTimeZone("UTC")
            d = s.parse(dateString)
        } catch (pe: ParseException) { //try again with optional decimals
            val s = SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'",
                Locale.getDefault()
            ) //spec for RFC3339 with a 'Z' and fractional seconds
            s.timeZone = TimeZone.getTimeZone("UTC")
            s.isLenient = true
            d = s.parse(dateString)
        }
        return d
    }

    //step one, split off the timezone.
    val firstPart: String
    var secondPart: String
    if (dateString.lastIndexOf('+') == -1) {
        firstPart = dateString.substring(0, dateString.lastIndexOf('-'))
        secondPart = dateString.substring(dateString.lastIndexOf('-'))
    } else {
        firstPart = dateString.substring(0, dateString.lastIndexOf('+'))
        secondPart = dateString.substring(dateString.lastIndexOf('+'))
    }

    //step two, remove the colon from the timezone offset
    secondPart = secondPart.substring(0, secondPart.indexOf(':')) + secondPart.substring(
        secondPart.indexOf(':') + 1
    )
    dateString = firstPart + secondPart
    var s =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault()) //spec for RFC3339
    try {
        d = s.parse(dateString)
    } catch (pe: ParseException) { //try again with optional decimals
        s = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ",
            Locale.getDefault()
        ) //spec for RFC3339 (with fractional seconds)
        s.isLenient = true
        d = s.parse(dateString)
    }
    return d
}

fun formatDate(pattern: String): String {
    val calendar = Calendar.getInstance(Locale.getDefault())
    return DateFormat.format(pattern, calendar).toString()
}

fun formatDate(calendar: Calendar, pattern: String): String =
    DateFormat.format(pattern, calendar).toString()

fun convertedDate(outputPattern: String, stringDate: Date): String {
    val targetFormat = SimpleDateFormat(outputPattern, Locale.US)
    return targetFormat.format(stringDate)
}

fun getCurrentDateIn3999(): String {
    val formatDate =
        formatDate("yyyy-MM-dd kk:mm:ss")
    val splitter = formatDate.split(" ")
    return splitter[0]+"T"+splitter[1]+".000+07:00"
}

fun getCurrentDateStandard(): String{
    return formatDate("MM/dd/yyyy")
}

fun getFormattedDateStandard(rfcDate: String): String{
    val removeMilisecond = rfcDate.removeSuffix(".000Z")
    val fromRfc = parseRFC3339Date(
        "$removeMilisecond+07:00"
    )!!
    val toDatePattern = "MM/dd/yyyy"
    return convertedDate(toDatePattern, fromRfc)
}

fun getFormattedDate(rfcDate: String): String{
    var result = ""
    val removeMilisecond = rfcDate.removeSuffix(".000Z")
    val fromRfc = parseRFC3339Date(
        "$removeMilisecond+07:00"
    )!!
    val toDatePattern = "EEEE dd MMMM yyyy"
    result = convertedDate(toDatePattern, fromRfc)
    val arr = result.split(" ")
    when(arr[0]){
        "Sunday" -> { result = result.replace("Sunday", "Minggu") }
        "Monday" -> { result = result.replace("Monday", "Senin") }
        "Tuesday" -> { result = result.replace("Tuesday", "Selasa") }
        "Wednesday" -> { result =  result.replace("Wednesday", "Rabu") }
        "Thursday" -> { result = result.replace("Thursday", "Kamis") }
        "Friday" -> { result = result.replace("Friday", "Jum\'at") }
        "Saturday" -> { result =result.replace("Saturday", "Sabtu") }
    }
    when(arr[2]){
        "January" -> { result = result.replace("January", "Januari") }
        "February" -> { result = result.replace("February", "Februari") }
        "March" -> { result = result.replace("March", "Maret") }
        "May" -> { result = result.replace("May", "Mei") }
        "June" -> { result = result.replace("June", "Juni") }
        "July" -> { result = result.replace("July", "Juli") }
        "August" -> { result = result.replace("August", "Agustus") }
        "October" -> { result = result.replace("October", "Oktober") }
        "December" -> { result = result.replace("December", "Desember") }
    }
    val finalSplit = result.split(" ")
    return finalSplit[0] + ", " + finalSplit[1] + " " + finalSplit[2] + " " + finalSplit[3]
}

fun getFormattedTime(rfcDate: String): String{
    val fromRfc = parseRFC3339Date(
        rfcDate.replace("Z", "+07:00")
    )!!
    val toTimePattern = "kk:mm"
    return convertedDate(toTimePattern, fromRfc)
}