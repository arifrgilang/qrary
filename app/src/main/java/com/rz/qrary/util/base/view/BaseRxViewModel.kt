package com.rz.qrary.util.base.view

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseRxViewModel : ViewModel(), LifecycleObserver {
    private val disposables by lazy { CompositeDisposable() }

    fun addToDisposable(disposable: Disposable){
        disposables.add(disposable)
    }

    override fun onCleared() {
        with(disposables){
            clear()
            dispose()
        }
        super.onCleared()
    }
}