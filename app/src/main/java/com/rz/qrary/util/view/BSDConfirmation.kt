package com.rz.qrary.util.view

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.ext.getProgressDrawable
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.loadImage
import com.rz.qrary.util.ext.visible
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bsd_confirmation.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class BSDConfirmation (
    private val urlFoto: String,
    private val msgHeader: String,
    private val msgYes: String,
    private val msgNo: String,
    private val callback: OnConfirmationCallback?
) : BottomSheetDialogFragment(){

    companion object{
        @JvmStatic
        private fun newInstance(
            urlFoto: String,
            msgHeader: String,
            msgYes: String,
            msgNo: String,
            callback: OnConfirmationCallback?
        ) = BSDConfirmation(urlFoto, msgHeader, msgYes, msgNo, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bsd_confirmation, container, false)
        if(urlFoto.isEmpty()){
            view.ivConfirmation.gone()
        } else {
            view.ivConfirmation.visible()
            view.ivConfirmation.loadImage(urlFoto, getProgressDrawable(requireContext()))
        }
        view.tvConfirmationHeader.text = msgHeader
        view.btnConfirmationYa.text = msgYes
        view.btnConfirmationBatal.text = Html.fromHtml("<u>$msgNo</u>")
        view.btnConfirmationBatal.onClick {
            dismiss()
        }
        view.btnConfirmationYa.onClick {
            dismiss()
            callback?.onUserAgreed()
        }
        return view
    }

    object Builder{
        private var urlFoto = ""
        private var msgHeader = "Anda yakin?"
        private var msgBtnYes = "Ya"
        private var msgBtnNo = "Tidak"
        private var callback: OnConfirmationCallback? = null

        fun headerMessage(msg: String): Builder{
            msgHeader = msg
            return this
        }

        fun yesButtonMessage(msg: String): Builder{
            msgBtnYes = msg
            return this
        }

        fun noButtonMessage(msg: String): Builder{
            msgBtnNo = msg
            return this
        }

        fun urlFoto(url: String): Builder{
            urlFoto = url
            return this
        }

        fun callback(cb: OnConfirmationCallback): Builder{
            callback = cb
            return this
        }

        fun build(): BSDConfirmation{
            return newInstance(
                urlFoto, msgHeader, msgBtnYes, msgBtnNo, callback
            )
        }
    }

    interface OnConfirmationCallback{
        fun onUserAgreed()
    }
}