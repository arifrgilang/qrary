package com.rz.qrary.util.ext

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.webkit.WebView
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.rz.qrary.R
import java.lang.IllegalArgumentException

fun View.goneAlpha() {
    visibility = View.GONE
    animate().alpha(0f).duration = 300
}

fun View.visibleAlpha() {
    visibility=View.VISIBLE
    animate().alpha(1f).duration = 300
}

fun getProgressDrawable(context: Context): CircularProgressDrawable {
    return CircularProgressDrawable(context).apply {
        strokeWidth = 10f
        centerRadius = 50f
        start()
    }
}

fun getSmallProgressDrawable(context: Context): CircularProgressDrawable {
    return CircularProgressDrawable(context).apply {
        strokeWidth = 4f
        centerRadius = 24f
        start()
    }
}

fun ImageView.loadImage(uri: String?, progressDrawable: CircularProgressDrawable){
    val options = RequestOptions()
        .placeholder(progressDrawable)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .priority(Priority.HIGH)
        .dontAnimate()
        .dontTransform()

    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .error(ContextCompat.getDrawable(context, R.drawable.error_no_picture))
        .into(this)
}

fun ImageView.loadBadge(uri: String?, progressDrawable: CircularProgressDrawable){
    val options = RequestOptions()
    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .into(this)
}

fun ImageView.loadImageCircle(uri: String?, progressDrawable: CircularProgressDrawable){
    val options = RequestOptions()
        .placeholder(progressDrawable)
        .circleCrop()
    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .error(ContextCompat.getDrawable(context, R.drawable.error_no_picture))
        .into(this)
}

fun ImageView.loadDrawable(drawable: Drawable?){
    val options = RequestOptions()
        .centerCrop()
        .error(R.mipmap.ic_launcher)
    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(drawable)
        .into(this)
}

fun ImageView.circleDrawable(drawable: Drawable?){
    val options = RequestOptions()
        .error(R.mipmap.ic_launcher)
        .circleCrop()
    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(drawable)
        .into(this)
}

fun ImageView.loadBitmap(bitmap: Bitmap?){
    val options = RequestOptions()
        .error(R.mipmap.ic_launcher)
    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(bitmap)
        .into(this)
}

fun ImageView.loadPath(path: String){

    val myBitmap = BitmapFactory.decodeFile(path)
    val options = RequestOptions()
        .error(R.mipmap.ic_launcher)
        .centerCrop()
    Glide.with(context)
        .setDefaultRequestOptions(options)
        .load(myBitmap)
        .into(this)
}

fun WebView.loadHTML(html: String?){
    this.loadDataWithBaseURL(null,
        "<style>img {width: 100%; height: auto;}</style>$html", "text/html", "UTF-8", null)
}


/** makes visible a view. */
fun View.visible() {
    visibility = View.VISIBLE
}

/** makes gone a view. */
fun View.gone() {
    visibility = View.GONE
}

fun Button.enable() {
    isEnabled = true
}

fun Button.disable() {
    isEnabled = false
}

inline fun EditText.onTextChange(crossinline f: (s: CharSequence, start: Int, before: Int, count: Int) -> Unit) {
    val listener = object : KoiTextWatcher() {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            f(s, start, before, count)
        }
    }
    this.addTextChangedListener(listener)
}

abstract class KoiTextWatcher : TextWatcher {
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {}
}

fun Context.color(id: Int): Int = ContextCompat.getColor(this, id)

fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

//Crete Name Space for XML
@BindingAdapter("android:imageUrl")
fun loadImage(view: ImageView, url: String?){
    view.loadImage(url,
        getProgressDrawable(view.context)
    )
}

@BindingAdapter("android:badgeUrl")
fun loadBadge(view: ImageView, url: String?){
    view.loadBadge(url,
        getProgressDrawable(view.context)
    )
}

@BindingAdapter("android:circleImageUrl")
fun loadCircleImage(view: ImageView, url: String?){
    view.loadImageCircle(url,
        getSmallProgressDrawable(view.context)
    )
}

@BindingAdapter("android:loadHTML")
fun loadHtml(view: WebView, html: String){
    view.loadHTML(html)
}