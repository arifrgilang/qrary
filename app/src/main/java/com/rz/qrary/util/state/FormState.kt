package com.rz.qrary.util.state

sealed class FormState

object Valid : FormState()
object Invalid : FormState()
//class Error(val error: Throwable) : UiState()