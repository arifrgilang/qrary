package com.rz.qrary.util.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.rz.qrary.R
import kotlinx.android.synthetic.main.spinner_dialog.view.*

class SpinnerDialog(
    private val et: EditText,
    private val dialogTitle: String,
    private val stringList: ArrayList<String>
) : DialogFragment() {
    private lateinit var adapter: ArrayAdapter<String>
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.spinner_dialog, null)
        val builder = AlertDialog.Builder(requireContext()).setView(view)
        setupAdapter(view)
        return builder.create()
    }

    private fun setupAdapter(view: View){
        adapter = ArrayAdapter(requireContext(),
            android.R.layout.simple_list_item_1,
            stringList
        )
        view.tvSpinnerDialog.text = dialogTitle
        view.lvSpinnerDialog.adapter = adapter
        view.lvSpinnerDialog.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                adapter.getItem(position)?.let{ et.setText(it) }
                dismiss()
            }
    }
}