package com.rz.qrary.util.base

interface AdapterOnClick {
    fun onRecyclerItemClicked(extra: String)
}