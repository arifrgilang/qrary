package com.rz.qrary.presentation.admin.returnbook

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rz.qrary.R
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.loan.MPeminjam
import com.rz.qrary.databinding.ActivityReturnBookBinding
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.toast
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.other.CustomRvMargin
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.util.view.BSDConfirmation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

@AndroidEntryPoint
class ReturnBookActivity : BaseBindingActivity<ActivityReturnBookBinding>() {
    private val viewModel: ReturnBookViewModel by viewModels()
    @Inject lateinit var rvAdapter: ReturnBookRvAdapter
    private lateinit var ticketId: String

    override fun contentView(): Int = R.layout.activity_return_book

    override fun setupData(savedInstanceState: Bundle?) {
        ticketId = intent.getStringExtra("ticketId")!!
    }

    override fun setupView() {
        setupToolbar("PENGEMBALIAN BUKU", true)
        initRv()
        observeViewModel()
        setupViewListener()
        viewModel.getHistory(ticketId)
    }

    private fun setupViewListener() {
        binding.btnReturnBook.onClick {
            showBSDConfirmation()
        }
        binding.btnReturnBookKeluar.onClick {
            showQuitConfirmation()
        }
        toolbar_back.onClick {
            showQuitConfirmation()
        }
    }

    private fun initRv() {
        with(binding.rvReturnBook){
            adapter = rvAdapter
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(this@ReturnBookActivity)
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    this@ReturnBookActivity,
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.historyResponse, ::onHistoryResponse)
        observe(viewModel.returnBookResponse, ::onReturnSuccess)
    }

    private fun onReturnSuccess(response: ResponseObject<MPeminjam>) {
        response.data?.let{
            toast(response.message!!)
            if(it.isDikembalikan!!) finish()
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.rlReturnBookLoading.gone()
        binding.rlReturnBookView.visible()
    }

    private fun showLoadingState() {
        binding.rlReturnBookView.gone()
        binding.rlReturnBookLoading.visible()
    }

    private fun onHistoryResponse(response: ResponseArray<MHistoryDetail>) {
        response.data?.let{
            binding.user = it[0].user
            rvAdapter.clearAndNotify()
            rvAdapter.insertAndNotify(it[0].books)
        }
    }

    private fun showQuitConfirmation() {
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin ingin keluar?")
            .yesButtonMessage("Ya")
            .noButtonMessage("Batal")
            .callback(
                object: BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        finish()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmation")
    }

    private fun showBSDConfirmation(){
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin?")
            .yesButtonMessage("Ya")
            .noButtonMessage("KEMBALI")
            .callback(
                object: BSDConfirmation.OnConfirmationCallback {
                    override fun onUserAgreed() {
                        viewModel.returnBook(ticketId)
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmation")
    }
    override fun onBackPressed() {
        showQuitConfirmation()
    }
}
