package com.rz.qrary.presentation.admin.editbook

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.model.book.MBook
import com.express.domain.qrary.Repository
import com.express.domain.model.book.MAddBook
import com.express.domain.model.book.MUpdateBook
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*
import okhttp3.MultipartBody

class EditBookViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel() {
    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _updateResponse = MutableLiveData<ResponseObject<MBook>>()
    val updateResponse: LiveData<ResponseObject<MBook>>
        get() = _updateResponse

    private val _updateCoverResponse = MutableLiveData<ResponseObject<MBook>>()
    val updateCoverResponse: LiveData<ResponseObject<MBook>>
        get() = _updateCoverResponse

    fun validateForm(
        judul: String,
        penulis: String,
        penerbit: String,
        bahasa: String,
        deskripsiBuku: String,
        kategoriBuku: String,
        isImageUp: Boolean
    ){
        _uiState.value = Loading
        if(
            judul.isEmpty() ||
            penulis.isEmpty() ||
            penerbit.isEmpty() ||
            bahasa.isEmpty() ||
            deskripsiBuku.isEmpty() ||
            kategoriBuku.isEmpty() ||
            !isImageUp
        ){
            _formState.value = Invalid
            _uiState.value = Success
        } else {
            _formState.value = Valid
        }
    }

    fun updateBook(bookId: String, book: MUpdateBook){
        addToDisposable(
            repository
                .updateBook(bookId, book)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _updateResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun updateCover(bookId: String, image: MultipartBody.Part) {
        _uiState.value = Loading
        addToDisposable(
            repository
                .updateCover(bookId, image)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _uiState.value = Success
                        _updateCoverResponse.value = it
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}