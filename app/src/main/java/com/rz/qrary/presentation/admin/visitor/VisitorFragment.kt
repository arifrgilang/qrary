package com.rz.qrary.presentation.admin.visitor

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rz.qrary.R
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.visitor.MVisitorResponse
import com.rz.qrary.databinding.FragmentVisitorBinding
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.other.CustomRvMargin
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

@AndroidEntryPoint
class VisitorFragment : BaseBindingFragment<FragmentVisitorBinding>(){
    private val viewModel: VisitorViewModel by viewModels()
    @Inject lateinit var rvAdapter: VisitorRvAdapter

    override fun contentView(): Int = R.layout.fragment_visitor

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        initRv()
        setupViewListener()
        observeViewModel()
        viewModel.getVisitor()
    }

    private fun setupViewListener() {
        binding.srVisitor.onRefresh {
            viewModel.getVisitor()
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.visitorResponse, ::onResponse)
    }

    private fun onResponse(response: ResponseArray<MVisitorResponse>) {
        with(rvAdapter){
            clearAndNotify()
            insertAndNotify(response.data)
        }
        Log.d("ITEM COUNT", rvAdapter.itemCount.toString())
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun showLoadingState() {
        binding.srVisitor.isRefreshing = true
    }

    private fun hideLoadingState() {
        binding.srVisitor.isRefreshing = false
    }

    private fun initRv() {
        with(binding.rvVisitor){
            adapter = rvAdapter
            layoutManager = LinearLayoutManager(requireContext())
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    requireContext(),
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }
}
