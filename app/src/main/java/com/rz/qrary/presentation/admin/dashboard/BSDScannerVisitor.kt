package com.rz.qrary.presentation.admin.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.express.domain.model.account.MUser
import com.rz.qrary.presentation.ScannerCallback
import com.rz.qrary.util.ext.getSmallProgressDrawable
import com.rz.qrary.util.ext.loadImage
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bsd_pengunjung.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class BSDScannerVisitor(
    private val user: MUser,
    private val date: String,
    private val time: String,
    private val callback: ScannerCallback,
    private val requestCode: Int
) : BottomSheetDialogFragment(){

    companion object {
        @JvmStatic
        fun newInstance(user: MUser,
                        date: String = "",
                        time: String = "",
                        callback: ScannerCallback,
                        requestCode: Int
        ) = BSDScannerVisitor(user, date, time, callback, requestCode)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bsd_pengunjung, container, false)
        view.tvBSDVisitorName.text = user.nama
        view.tvBSDVisitorNPM.text = user.npm
        view.tvBSDVisitorDate.text = date
        view.tvBSDVisitorTime.text = "Pukul $time"
        view.ivVisitor.loadImage(user.urlFoto, getSmallProgressDrawable(requireContext()))
        view.btnScanAgainVisitor.onClick {
            dismiss()
            callback.startScanner(requestCode)
        }
        view.tvBSDVisitorKeluar.onClick {
            dismiss()
        }

        return view
    }
}