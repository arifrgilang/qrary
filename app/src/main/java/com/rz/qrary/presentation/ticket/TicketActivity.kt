package com.rz.qrary.presentation.ticket

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rz.qrary.R
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.rz.qrary.databinding.ActivityTicketBinding
import com.rz.qrary.util.other.AppUtils
import com.express.domain.qrary.SCAN_ADD_BUKU_PINJAMAN
import com.express.domain.model.account.MUser
import com.express.domain.model.book.MBook
import com.express.domain.model.loan.MPeminjam
import com.express.domain.model.ticket.MAddBookTicket
import com.express.domain.model.ticket.MPeminjaman
import com.express.domain.model.ticket.MTicketDetails
import com.rz.qrary.util.ext.*
import com.rz.qrary.util.other.CustomRvMargin
import com.rz.qrary.util.state.*
import com.rz.qrary.util.view.BSDConfirmation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.toolbar.*
import me.ydcool.lib.qrmodule.activity.QrScannerActivity
import org.jetbrains.anko.longToast
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

@AndroidEntryPoint
class TicketActivity : BaseBindingActivity<ActivityTicketBinding>() {
    private val viewModel: TicketViewModel by viewModels()
    @Inject lateinit var rvAdapter: TicketRvAdapter
    private lateinit var user: MUser
    private lateinit var ticketId: String

    override fun contentView(): Int = R.layout.activity_ticket

    override fun setupData(savedInstanceState: Bundle?) {
        with(intent){
            user = getSerializableExtra("pinjamUser") as MUser
            ticketId = getStringExtra("pinjamTicketId")!!
        }
        binding.user = user
    }

    override fun setupView() {
        setupToolbar("Konfirmasi Peminjaman", true)
        initRv()
        observeViewModel()
        setupViewListener()
        viewModel.getListBook(ticketId)
    }

    private fun setupViewListener() {
        binding.btnTicketAddBook.onClick {
            startScanner(SCAN_ADD_BUKU_PINJAMAN)
        }

        binding.btnTicketFinish.onClick {
            viewModel.checkRequirements(rvAdapter.itemCount)
        }

        binding.btnTicketKeluar.onClick {
            showQuitConfirmation()
        }

        toolbar_back.onClick {
            showQuitConfirmation()
        }
    }

    private fun startScanner(requestCode: Int) {
        startActivityForResult(
            Intent(
                this@TicketActivity, QrScannerActivity::class.java
            ), requestCode
        )
        longToast("Scan ISBN buku yang akan dipinjam")
    }

    private fun initRv() {
        with(binding.rvTicket){
            isNestedScrollingEnabled = false
            adapter = rvAdapter
                .apply {
                    setOnDeleteListener(
                        object: TicketRvAdapter.OnClickDelete{
                            override fun onBookDeleteConfirmation(isbn: String, urlFoto: String) {
                                showDeleteConfirmation(isbn, urlFoto)
                            }
                        }
                    )
                }
            layoutManager = LinearLayoutManager(this@TicketActivity)
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    this@TicketActivity,
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }

    private fun observeViewModel() {
        observe(viewModel.rvUiState, ::handleLoadingRv)
        observe(viewModel.rvResponse, ::onResponseRv)
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.uiResponse, ::onResponseAddBook)
        observe(viewModel.formState, ::onFormState)
        observe(viewModel.savePeminjamResponse, ::onResponsePeminjam)
        observe(viewModel.dateState, ::onDateState)
        observe(viewModel.deleteBookResponse, ::onDeleteBookResponse)
    }

    private fun onDateState(state: FormState) {
        when(state){
            is Invalid -> toast("Rentang tanggal harus 1-14 hari!")
        }
    }

    private fun onResponsePeminjam(response: ResponseObject<MPeminjam>) {
        response.total?.let{
            if(it>0){
                toast("Peminjaman Berhasil")
                finish()
            }
        }
    }

    private fun onFormState(state: FormState) {
        when(state){
            is Invalid -> toast("Daftar buku masih kosong!")
            is Valid -> showBSDDate()
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun handleLoadingRv(state: UiState) {
        when(state){
            is Loading -> showLoadingRvState()
            is Success -> hideLoadingRvState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun onResponseAddBook(response: ResponseObject<MBook>) {
        response.let{
            if(it.total!! > 0){
                viewModel.getListBook(ticketId)
                showBSDNewAdded(it.data!!.urlCover!!)
            }
            val msg = response.message
            toast("$msg")
        }
    }

    private fun onResponseRv(response: ResponseArray<MTicketDetails>) {
        response.data?.let {
            rvAdapter.clearAndNotify()
            if(it[0].listBuku!!.isEmpty()){
                showListEmptyState()
            } else {
                rvAdapter.insertAndNotify(it[0].listBuku)
            }
        }
    }

    private fun onDeleteBookResponse(response: ResponseArray<MBook>) {
        if(response.total!! >0){
            showDeletedBook(response.data!![0].urlCover!!)
            viewModel.getListBook(ticketId)
        }
    }

    private fun hideLoadingState() {
        binding.rlTicketLoading.gone()
        binding.rlTicketView.visible()
    }

    private fun showLoadingState() {
        binding.rlTicketView.gone()
        binding.rlTicketLoading.visible()
    }

    private fun hideLoadingRvState() {
        binding.pbTicket.gone()
        binding.rvTicket.visible()
        binding.btnTicketFinish.enable()
        binding.btnTicketAddBook.isEnabled = true
    }

    private fun showLoadingRvState() {
        binding.rvTicket.gone()
        binding.rlEmptyTicket.gone()
        binding.pbTicket.visible()
        binding.btnTicketFinish.disable()
        binding.btnTicketAddBook.isEnabled = false
    }

    private fun showListEmptyState() {
        binding.rvTicket.gone()
        binding.rlEmptyTicket.visible()
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingRvState()
        hideLoadingState()
        toast("${error.message}")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                SCAN_ADD_BUKU_PINJAMAN -> {
                    val isbn = data!!.extras!!.getString(QrScannerActivity.QR_RESULT_STR)!!
                    viewModel.addBookToTicket(
                        MAddBookTicket(
                            isbn,
                            ticketId
                        )
                    )
                }
            }
        } else {
            toast("Scan dibatalkan")
        }
    }

    private fun showBSDDate() {
        BSDDate.newInstance(
            object: BSDDate.BSDDateCallback{
                override fun onGetDate(dateFrom: String, dateTo: String) {
                    viewModel.savePeminjaman(
                        MPeminjaman(
                            ticketId,
                            dateFrom,
                            dateTo
                        )
                    )
                }
            }
        ).show(supportFragmentManager, "BSDDate")
    }

    private fun showQuitConfirmation() {
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin ingin keluar?")
            .yesButtonMessage("Ya")
            .noButtonMessage("Batal")
            .callback(
                object: BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        finish()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmation")
    }

    private fun showBSDNewAdded(urlFoto: String) {
        BSDConfirmation.Builder
            .urlFoto(urlFoto)
            .headerMessage("Buku baru berhasil ditambahkan!")
            .yesButtonMessage("Tambah buku lain")
            .noButtonMessage("KELUAR")
            .callback(
                object : BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        binding.btnTicketAddBook.performClick()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmationAdded")
    }

    private fun showDeleteConfirmation(isbn: String, urlFoto: String) {
        BSDConfirmation.Builder
            .urlFoto(urlFoto)
            .headerMessage("Hapus buku ini?")
            .yesButtonMessage("YA, HAPUS BUKU INI")
            .noButtonMessage("BATAL")
            .callback(
                object : BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        viewModel.deleteBookFromModel(isbn, ticketId)
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmationDeleteBook")
    }


    private fun showDeletedBook(urlFoto: String) {
        AppUtils.showBSD(R.drawable.bsd_success,
            "Buku berhasil dihapus",
            supportFragmentManager
        )
    }

    override fun onBackPressed() {
        showQuitConfirmation()
    }
}
