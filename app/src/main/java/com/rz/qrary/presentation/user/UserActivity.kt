package com.rz.qrary.presentation.user

import android.content.Intent
import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.rz.qrary.databinding.ActivityUserBinding
import com.rz.qrary.presentation.user.booklist.dipinjam.BookListFragment
import com.rz.qrary.presentation.user.home.HomeFragment
import com.rz.qrary.presentation.user.home.QRDialogFragment
import com.rz.qrary.presentation.user.library.LibraryFragment
import com.rz.qrary.presentation.user.profile.ProfileFragment
import com.express.domain.qrary.BACK_PRESSED_INTERVAL
import com.express.domain.qrary.NPM
import com.rz.qrary.util.ext.toast
import com.express.domain.qrary.IS_USER_LOGIN
import com.express.domain.qrary.USER_TOKEN
import com.rz.qrary.util.view.BSDConfirmation
import com.rz.qrary.presentation.LogoutCallback
import com.rz.qrary.presentation.login.LoginActivity
import com.orhanobut.hawk.Hawk
import com.rz.qrary.util.ext.start
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserActivity : BaseBindingActivity<ActivityUserBinding>(), LogoutCallback {
    private var mBackPressed: Long = 0

    override fun contentView(): Int = R.layout.activity_user

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupViewListener()
        showHomeFragment()
    }

    private fun setupViewListener() {
        binding.bottomNavigationUser.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.userBeranda -> showHomeFragment()
                R.id.userInfoBuku -> showLibraryFragment()
                R.id.userDaftar -> showBookListFragment()
                R.id.userProfile -> showProfileFragment()
            }
            true
        }
    }

    private fun showHomeFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flUser, HomeFragment::class.java, null)
            .commit()
    }

    private fun showLibraryFragment(){
        val a = LibraryFragment::class.java
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flUser, LibraryFragment::class.java, null)
            .commit()
    }

    private fun showBookListFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flUser, BookListFragment::class.java, null)
            .commit()
    }

    private fun showProfileFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flUser, ProfileFragment::class.java, null)
            .commit()
    }

    fun showQrDialog() {
        val dialog = QRDialogFragment(Hawk.get(NPM))
        dialog.show(supportFragmentManager, "QR Dialog")
    }

    override fun onBackPressed() {
        if(mBackPressed + BACK_PRESSED_INTERVAL > System.currentTimeMillis()){
            super.onBackPressed()
        } else{
            toast("Tekan tombol kembali 2 kali untuk keluar dari aplikasi")
        }
        mBackPressed = System.currentTimeMillis()
    }

    override fun showBSDConfirmationLogout() {
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin ingin keluar?")
            .yesButtonMessage("Ya")
            .noButtonMessage("BATAL")
            .callback(
                object : BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        logoutUser()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDLogoutUser")
    }

    private fun logoutUser(){
        Hawk.delete(USER_TOKEN)
        Hawk.put(IS_USER_LOGIN, "")
        start<LoginActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }
}
