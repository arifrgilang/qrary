package com.rz.qrary.presentation.admin.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.express.domain.model.account.MUser
import com.rz.qrary.util.ext.getSmallProgressDrawable
import com.rz.qrary.util.ext.loadImage
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bsd_peminjam.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class BSDScannerPeminjam(
    private val user: MUser,
    private val ticketId: String,
    private val callback: PeminjamCallback
) : BottomSheetDialogFragment(){

    companion object{
        @JvmStatic
        fun newInstance(
            user: MUser,
            ticketId: String,
            callback: PeminjamCallback
        ) = BSDScannerPeminjam(user, ticketId, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bsd_peminjam, container, false)
        view.tvBSDPeminjamName.text = user.nama
        view.tvBSDPeminjamNPM.text = user.npm
        view.ivPeminjam.loadImage(user.urlFoto, getSmallProgressDrawable(requireContext()))
        view.btnScanAgainPeminjam.onClick {
            dismiss()
            callback.startPeminjamActivity(user, ticketId)
        }
        view.tvBSDPeminjamKeluar.onClick {
            dismiss()
        }
        return view
    }

    interface PeminjamCallback{
        fun startPeminjamActivity(user: MUser, ticketId: String)
    }
}