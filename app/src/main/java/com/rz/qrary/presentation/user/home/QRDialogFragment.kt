package com.rz.qrary.presentation.user.home

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.rz.qrary.R
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import kotlinx.android.synthetic.main.qr_dialog.view.*
import me.ydcool.lib.qrmodule.encoding.QrGenerator

class QRDialogFragment(private val npm: String): DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view =
            LayoutInflater
                .from(requireContext()).
                inflate(R.layout.qr_dialog, null)
        val builder =
            AlertDialog
                .Builder(requireContext())
                .setView(view)
        view.ivQRCode.setImageBitmap(generateQRCode())
        return builder.create()
    }

    private fun generateQRCode(): Bitmap =
        QrGenerator.Builder()
            .content("qrary"+npm)
            .qrSize(800)
            .color(Color.BLACK)
            .ecc(ErrorCorrectionLevel.H)
            .encode()
}