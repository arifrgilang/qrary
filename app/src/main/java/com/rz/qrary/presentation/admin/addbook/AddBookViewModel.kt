package com.rz.qrary.presentation.admin.addbook

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseObject
import com.express.domain.model.book.MBook
import com.express.domain.qrary.Repository
import com.express.domain.model.book.MAddBook
import com.rz.qrary.util.ext.createMultipart
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*
import okhttp3.MultipartBody
import okhttp3.RequestBody

class AddBookViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _addBookResponse = MutableLiveData<ResponseObject<MBook>>()
    val addBookResponse: LiveData<ResponseObject<MBook>>
        get() = _addBookResponse

    private val _updateCoverResponse = MutableLiveData<ResponseObject<MBook>>()
    val updateCoverResponse: LiveData<ResponseObject<MBook>>
        get() = _updateCoverResponse

    fun validateForm(
        judul: String,
        penulis: String,
        penerbit: String,
        bahasa: String,
        deskripsiBuku: String,
        kategoriBuku: String,
        isCoverAdded: Boolean
    ){
        _uiState.value = Loading
        if(
            judul.isEmpty() ||
            penulis.isEmpty() ||
            penerbit.isEmpty() ||
            bahasa.isEmpty() ||
            deskripsiBuku.isEmpty() ||
            kategoriBuku.isEmpty() ||
            !isCoverAdded
        ){
            _formState.value = Invalid
            _uiState.value = Success
        } else {
            _formState.value = Valid
        }
    }

    fun addBook(map: HashMap<String, RequestBody>, image: MultipartBody.Part){
        _uiState.value = Loading
        addToDisposable(
            repository
                .addBook(map, image)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _uiState.value = Success
                        _addBookResponse.value = it
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}