package com.rz.qrary.presentation.admin.returnbook

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.loan.MPeminjam
import com.express.domain.model.loan.MReturnBookKey
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState

class ReturnBookViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _historyResponse = MutableLiveData<ResponseArray<MHistoryDetail>>()
    val historyResponse: LiveData<ResponseArray<MHistoryDetail>>
        get() = _historyResponse

    private val _returnBookResponse = MutableLiveData<ResponseObject<MPeminjam>>()
    val returnBookResponse: LiveData<ResponseObject<MPeminjam>>
        get() = _returnBookResponse

    fun getHistory(ticketId: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getSpesificPeminjaman("_id", ticketId)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _historyResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun returnBook(ticketId: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .returnBook(MReturnBookKey(ticketId))
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _returnBookResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}