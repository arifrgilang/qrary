package com.rz.qrary.presentation.history

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.loan.MHistoryDetail
import com.rz.qrary.databinding.ActivityHistoryBinding
import com.rz.qrary.util.other.CustomRvMargin
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.presentation.bookdetail.BookDetailActivity
import com.rz.qrary.util.ext.*
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HistoryActivity : BaseBindingActivity<ActivityHistoryBinding>() {
    private val viewModel: HistoryViewModel by viewModels()
    @Inject lateinit var rvAdapter: HistoryRvAdapter
    private lateinit var ticketId: String
    private lateinit var historyType: String

    override fun contentView(): Int = R.layout.activity_history

    override fun setupData(savedInstanceState: Bundle?) {
        with(intent){
            ticketId = getStringExtra("ticketId")!!
            historyType = getStringExtra("historyType")!!
        }
    }
    override fun setupView() {
        setupToolbar("DETAIL PEMINJAMAN", true)
        initRv()
        observeViewModel()
        viewModel.getHistory(historyType, ticketId)
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.historyResponse, ::onResponse)
    }

    private fun onResponse(response: ResponseArray<MHistoryDetail>) {
        response.data?.let{
            binding.user = it[0].user
            rvAdapter.clearAndNotify()
            rvAdapter.insertAndNotify(it[0].books)
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.rlHistoryLoading.gone()
        binding.rlHistoryView.visible()
    }

    private fun showLoadingState() {
        binding.rlHistoryView.gone()
        binding.rlHistoryLoading.visible()
    }

    private fun initRv() {
        with(binding.rvHistory){
            isNestedScrollingEnabled = false
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object : AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToBookDetail(extra)
                            }
                        }
                    )
                }
            layoutManager = LinearLayoutManager(this@HistoryActivity)
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    this@HistoryActivity,
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }

    private fun navigateToBookDetail(bookId: String) {
        start<BookDetailActivity>{
            putExtra("bookId", bookId)
        }
    }
}
