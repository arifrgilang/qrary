package com.rz.qrary.presentation.register

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.base.ResponseObject
import com.express.domain.model.account.MStudentData
import com.express.domain.model.account.MUser
import com.express.domain.model.auth.MTokenRole
import com.express.domain.qrary.IS_USER_LOGIN
import com.express.domain.qrary.NPM
import com.express.domain.qrary.OTP_REQUEST_CODE
import com.express.domain.qrary.USER_TOKEN
import com.orhanobut.hawk.Hawk
import com.rz.qrary.databinding.ActivityRegisterBinding
import com.rz.qrary.presentation.login.LoginActivity
import com.rz.qrary.presentation.otp.OtpActivity
import com.rz.qrary.presentation.user.UserActivity
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.other.AppUtils.Companion.showErrorIfEmpty
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.start
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.other.AppUtils
import com.rz.qrary.util.state.*
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

@AndroidEntryPoint
class RegisterActivity : BaseBindingActivity<ActivityRegisterBinding>() {
    private val viewModel: RegisterViewModel by viewModels()
    private lateinit var name: String

    override fun contentView(): Int = R.layout.activity_register
    override fun setupData(savedInstanceState: Bundle?) {}
    override fun setupView() {
        setupListener()
        observeViewModel()
    }

    private fun setupListener() {
        binding.tvDaftarMasuk.onClick {
            start<LoginActivity>{}
            finish()
        }

        binding.btnPeriksa.onClick {
            viewModel.validateNPM(binding.etDaftarNPM.text.toString())
        }
        binding.btnDaftar.onClick {
            viewModel.validateForm(
                binding.etDaftarEmail.text.toString(),
                binding.etDaftarSandi.text.toString(),
                binding.etDaftarSandiKonfirmasi.text.toString()
            )
        }
    }

    private fun observeViewModel() {
        observe(viewModel.formState, ::handleValidationForm)
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.registerResponse, :: onResponse)
        observe(viewModel.npmState, ::handleValidationNPM)
        observe(viewModel.npmResponse, ::onNpmResponse)
        observe(viewModel.loginResponse, ::onLoginResponse)
    }

    private fun onLoginResponse(response: ResponseObject<MTokenRole>) {
        response.data?.let{ authModel ->
            if(response.total!! > 0){
                saveCredentials(authModel)
                handleNavigation(authModel)
                toast(response.message.toString())
            }
        }
    }

    private fun handleNavigation(auth: MTokenRole){
        when(auth.role){
//            "superadmin" -> toDashboardAdmin()
            "user" -> toDashboardUser()
            else -> {}
        }
    }

    private fun saveCredentials(auth: MTokenRole){
        Hawk.put(USER_TOKEN, auth.token)
        Hawk.put(IS_USER_LOGIN, auth.role)
        Hawk.put(NPM, binding.etDaftarNPM.text.toString())
    }

    private fun toDashboardUser() {
        start<UserActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }

    private fun onNpmResponse(responseObject: ResponseObject<MStudentData>) {
        if (responseObject.total == 0){
            showBSD(R.drawable.bsd_failed, responseObject.message!!)
        } else {
            responseObject.data?.let{
                name = it.name!!
                binding.tvName.text = name
                binding.tvNPM.text = it.npm!!
            }
            changeToRegisterForm()
        }
    }

    private fun changeToRegisterForm() {
        showBSD(R.drawable.bsd_success, "NPM tersedia!")
        binding.llCheckNPM.gone()
        binding.llRegister.visible()
    }

    private fun handleValidationNPM(npmState: FormState) {
        when(npmState) {
            is Invalid -> handleInvalidForm()
            is Valid -> {
                viewModel.checkNPM(binding.etDaftarNPM.text.toString())
            }
        }
    }

    private fun handleValidationForm(state: FormState) {
        when(state){
            is Invalid -> handleInvalidForm()
            is Valid -> {
                viewModel.register(
                    name,
                    binding.etDaftarNPM.text.toString(),
                    binding.etDaftarEmail.text.toString(),
                    binding.etDaftarSandi.text.toString(),
                    binding.etDaftarSandiKonfirmasi.text.toString()
                )
            }
        }
    }

    private fun handleLoading(state: UiState){
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.pbDaftar.gone()
        binding.pbPeriksa.gone()
        binding.btnDaftar.visible()
        binding.btnPeriksa.visible()
    }

    private fun displayLoadingState() {
        binding.btnDaftar.gone()
        binding.pbPeriksa.gone()
        binding.pbDaftar.visible()
        binding.btnPeriksa.visible()
    }

    private fun onResponse(data: ResponseObject<MUser>){
        if(data.message == "Pendaftaran berhasil"){
            toOtpPage(data.data!!.email!!)
        } else {
            showBSD(
                R.drawable.bsd_warning,
                data.message.toString() + ". Silahkan masuk melalui halaman login."
            )
        }
    }

    private fun toOtpPage(email: String){
        startActivityForResult(
            Intent(this@RegisterActivity, OtpActivity::class.java)
                .apply {
                    putExtra("email", email)
                },
            OTP_REQUEST_CODE
        )
    }

    private fun showBSD(resId: Int, content: String) =
        AppUtils.showBSD(resId, content, supportFragmentManager)

    private fun handleInvalidForm() {
        showBSD(R.drawable.bsd_warning, getString(R.string.register_forminvalid))
        showErrorIfEmpty(binding.etDaftarEmail, getString(R.string.register_emailinvalid))
        if(binding.etDaftarEmail.text.toString().isNotEmpty() &&
            !AppUtils.isEmailValid(binding.etDaftarEmail.text.toString())){
            binding.etDaftarEmail.error = getString(R.string.register_emailunpad)
        }
        showErrorIfEmpty(binding.etDaftarNPM, getString(R.string.register_npminvalid))
        showErrorIfEmpty(binding.etDaftarSandi, getString(R.string.register_pwdinvalid))
        showErrorIfEmpty(binding.etDaftarSandiKonfirmasi, getString(R.string.register_pwdinvalid))
        if(!AppUtils.isPasswordValid(binding.etDaftarSandi.text.toString())){
            binding.etDaftarSandi.error =  getString(R.string.register_pwdinvalid)
        }
        if(!AppUtils.isPasswordValid(binding.etDaftarSandiKonfirmasi.text.toString())){
            binding.etDaftarSandiKonfirmasi.error = getString(R.string.register_pwdinvalid)
        }
        if(binding.etDaftarSandiKonfirmasi.text.toString() != binding.etDaftarSandi.text.toString()){
            binding.etDaftarSandiKonfirmasi.error = getString(R.string.register_pwdnotsame)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == OTP_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                viewModel.login(
                    binding.etDaftarNPM.text.toString(),
                    binding.etDaftarSandi.text.toString()
                )
            }
        }
    }
}
