package com.rz.qrary.presentation.otp

import android.os.CountDownTimer
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.model.auth.MEmail
import com.express.domain.model.auth.MVerifyOTP
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*
import java.util.*
import java.util.concurrent.TimeUnit

class OtpViewModel @ViewModelInject constructor(
    private val repository: Repository
): BaseRxViewModel() {
    private val _countdownTimer = MutableLiveData<String>()
    val countdownTimer: LiveData<String>
        get() = _countdownTimer

    private val _isCountdownFinished = MutableLiveData<Boolean>()
    val isCountdownFinished: LiveData<Boolean>
        get() = _isCountdownFinished

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _isOtpCorrect = MutableLiveData<FormState>()
    val isOtpCorrect: LiveData<FormState>
        get() = _isOtpCorrect

    private val _otpMessage = MutableLiveData<String>()
    val otpMessage: LiveData<String>
        get() = _otpMessage

    fun sendOTP(email: String){
        _uiState.value = Loading
        addToDisposable(
            repository.sendOTP(MEmail(email))
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _uiState.value = Success
                        setCountDown("01:00")
                    }, {
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun verifyOTP(otpModel: MVerifyOTP){
        _uiState.value = Loading
        addToDisposable(
            repository.verifyOTP(otpModel)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        val success = "Akun berhasil diaktivasi"
                        val failure = "Kode OTP Salah!"
                        when(it.message){
                            success -> {
                                _otpMessage.value = it.message
                                _uiState.value =
                                    Success
                                _isOtpCorrect.value =
                                    Valid
                            }
                            failure -> {
                                _otpMessage.value = it.message
                                _uiState.value =
                                    Success
                                _isOtpCorrect.value =
                                    Invalid
                            }
                        }
                    }, {
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    private fun setCountDown(time: String){
        _isCountdownFinished.postValue(false)
        val timeArray = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val minuteInMillis = TimeUnit.MINUTES.toMillis(timeArray[0].toLong())
        val secondInMillis = TimeUnit.SECONDS.toMillis(timeArray[1].toLong())

        val millis = minuteInMillis + secondInMillis

        object : CountDownTimer(millis, 1000){
            override fun onFinish() {
                _isCountdownFinished.postValue(true)
            }

            override fun onTick(millisUntilFinished: Long) {
                _countdownTimer.postValue(
                    String.format(
                        Locale.getDefault(),
                        "(%02d:%02d)",
                        millisToMinutes(millisUntilFinished),
                        millisToSecond(millisUntilFinished)
                    )
                )
            }
        }.start()
    }

    private fun millisToMinutes(value: Long) : Int = ((value / (1000*60)) % 60).toInt()
    private fun millisToSecond(value: Long) : Int = ((value / 1000) % 60).toInt()
}