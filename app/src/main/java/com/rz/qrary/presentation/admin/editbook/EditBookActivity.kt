package com.rz.qrary.presentation.admin.editbook

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.viewModels
import com.rz.qrary.R
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.book.MBook
import com.express.domain.model.book.MUpdateBook
import com.express.domain.qrary.GALLERY_REQUEST_CODE
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rz.qrary.databinding.ActivityEditBookBinding
import com.rz.qrary.util.ext.*
import com.rz.qrary.util.state.*
import com.rz.qrary.util.view.BSDConfirmation
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class EditBookActivity : BaseBindingActivity<ActivityEditBookBinding>() {
    private val viewModel: EditBookViewModel by viewModels()
    private lateinit var book: MBook
    private var kategori = ""
    private lateinit var imageBitmap: Bitmap
    private var isImageUp = false

    override fun contentView(): Int = R.layout.activity_edit_book

    override fun setupData(savedInstanceState: Bundle?) {
        book = intent.getSerializableExtra("book") as MBook
        book.kategori?.let{
            for(item in it){
                kategori+= "$item,"
            }
        }
        kategori = kategori.removeSuffix(",")
    }

    override fun setupView() {
        setupToolbar("EDIT BUKU", true)
        binding.book = book
        binding.kategori = kategori
        setupViewListener()
        observeViewModel()
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.formState, ::handleForm)
        observe(viewModel.updateResponse, ::onResponse)
        observe(viewModel.updateCoverResponse, ::onCoverUpdated)
    }

    private fun onCoverUpdated(response: ResponseObject<MBook>) {
        if(response.total!! > 0){
            toast("Cover buku berhasil diperbarui!")
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            toast("Cover buku gagal diperbarui!")
        }
    }

    private fun onResponse(response: ResponseObject<MBook>) {
        response.message?.let{
            toast(it)
            when(it){
                "Buku berhasil diperbarui" -> {
//                    setResult(Activity.RESULT_OK)
//                    finish()
                    toast("Data buku berhasil diperbarui")
                    viewModel.updateCover(
                        book.id!!,
                        createMultipart(this@EditBookActivity, imageBitmap)
                    )
                }
            }

        }
    }

    private fun handleForm(state: FormState) {
        when(state){
            is Invalid -> handleInvalidForm()
            is Valid -> handleValidForm()
        }
    }

    private fun handleValidForm() {
        val totalHal = if(
            binding.etTotalHalaman.text
                .toString()
                .isNotEmpty()
        ) binding.etTotalHalaman.text
            .toString()
            .toInt()
        else 0
        viewModel.updateBook(
            book.id!!,
            MUpdateBook(
                binding.etKategoriBuku.text.toString(),
                binding.etBahasa.text.toString(),
                book.isbn,
                totalHal,
                binding.etJudulBuku.text.toString(),
                binding.etDeskripsiBuku.text.toString(),
                binding.etPenerbit.text.toString(),
                binding.etPenulis.text.toString(),
                binding.etPenerjemah.text.toString(),
                binding.etTTTerbit.text.toString(),
                "Tersedia",
                book.urlCover
            )
        )
    }

    private fun handleInvalidForm() {
        toast("Lengkapi form!")
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("$error.message")
    }

    private fun hideLoadingState() {
        binding.pbEditBook.gone()
        binding.btnEditBook.visible()
    }

    private fun showLoadingState() {
        binding.btnEditBook.gone()
        binding.pbEditBook.visible()
    }

    private fun setupViewListener() {
        binding.btnAddCover.onClick {
            checkReadWritePermission()
        }
        binding.btnEditBook.onClick {
            showEditConfirmation()
        }
        toolbar_back.onClick {
            showQuitConfirmation()
        }
        binding.etTTTerbit.onClick {
            SpinnerDatePickerDialogBuilder()
                .context(this@EditBookActivity)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showDaySpinner(true)
                .defaultDate(2017, 0, 1)
                .callback { _, year, monthOfYear, dayOfMonth ->
                    binding.etTTTerbit.setText(
                        SimpleDateFormat("dd/MM/yyyy")
                            .format(Date(year-1900,monthOfYear,dayOfMonth))
                    )
                }
                .build()
                .show()
        }
    }

    private fun checkReadWritePermission() {
        Dexter
            .withActivity(this)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(
                object : PermissionListener {
                    override fun onPermissionGranted(
                        response: PermissionGrantedResponse?
                    ) {
                        takeGalleryIntent()
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(
                        response: PermissionDeniedResponse?
                    ) {
                        toast("Permission Denied")
                        finish()
                    }
                }
            )
            .check()
    }

    private fun takeGalleryIntent() {
        startActivityForResult(
            Intent(MediaStore.ACTION_IMAGE_CAPTURE),
            GALLERY_REQUEST_CODE
        )
    }

    private fun showEditConfirmation() {
        BSDConfirmation.Builder
            .urlFoto(book.urlCover!!)
            .yesButtonMessage("Ya")
            .noButtonMessage("BATAL")
            .headerMessage("Selesai mengubah info buku?")
            .callback(
                object: BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        validate()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "Confirmation Delete")
    }

    private fun validate(){
        viewModel.validateForm(
            binding.etJudulBuku.text.toString(),
            binding.etPenulis.text.toString(),
            binding.etPenerbit.text.toString(),
            binding.etBahasa.text.toString(),
            binding.etDeskripsiBuku.text.toString(),
            binding.etKategoriBuku.text.toString(),
            isImageUp
        )
    }

    private fun showQuitConfirmation() {
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin ingin keluar? Perubahan yang telah dibuat akan menghilang jika Anda keluar tanpa menyimpannya")
            .yesButtonMessage("Ya")
            .noButtonMessage("Batal")
            .callback(
                object: BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        finish()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmation")
    }

    override fun onBackPressed() {
        showQuitConfirmation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode to resultCode) {
            GALLERY_REQUEST_CODE to Activity.RESULT_OK -> {
                imageBitmap = data?.extras?.get("data") as Bitmap
                binding.ivDetailCover.setImageBitmap(imageBitmap)
                binding.ivDetailBgCover.setImageBitmap(imageBitmap)
                isImageUp = true
            }
        }
    }
}
