package com.rz.qrary.presentation.user.home

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.book.MBook
import com.express.domain.model.account.MUser
import com.rz.qrary.databinding.FragmentHomeBinding
import com.rz.qrary.util.ext.*
import com.rz.qrary.presentation.bookdetail.BookDetailActivity
import com.rz.qrary.presentation.user.UserActivity
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.presentation.article.ArticleActivity
import com.rz.qrary.presentation.searchbook.SearchBookActivity
import com.rz.qrary.util.other.CustomRvMargin
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.search_toolbar_user.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseBindingFragment<FragmentHomeBinding>() {
    private val viewModel: HomeViewModel by viewModels()
    @Inject lateinit var rvAdapter: HomeRvAdapter
//    @Inject lateinit var bannerRvAdapter: DiscreteHomeRvAdapter

    private val discreteList = mutableListOf<BannerModel>()

    override fun contentView(): Int = R.layout.fragment_home

    override fun setupData(savedInstanceState: Bundle?) {
        viewModel.getUserData()
        viewModel.getRecentBooks()
//        generateDiscreteViews()
    }

    override fun setupView() {
        initRv()
        setupViewListener()
        observeViewModel()
    }

    private fun initRv() {
        with(binding.rvRecentBooksUser){
            layoutManager = GridLayoutManager(requireContext(), 2)
            isNestedScrollingEnabled = false
            adapter = rvAdapter.apply {
                setOnItemClickListener(
                    object : AdapterOnClick {
                        override fun onRecyclerItemClicked(extra: String) {
                            navigateToDetail(extra)
                        }
                    }
                )
            }
            addItemDecoration(
                CustomRvMargin(
                    requireContext(),
                    16,
                    CustomRvMargin.GRID_2
                )
            )
        }
    }

    private fun navigateToDetail(bookId: String) {
        start<BookDetailActivity>{
            putExtra("bookId", bookId)
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.userProfileResponse, ::onResponse)
        observe(viewModel.rvState, ::handleRvLoading)
        observe(viewModel.recentBooksResponse, ::onRvResponse)
    }

    private fun onRvResponse(response: ResponseArray<MBook>) {
        response.data?.let{
            rvAdapter.clearAndNotify()
            rvAdapter.insertAndNotify(it)
        }
    }

    private fun onResponse(response: ResponseArray<MUser>) {
        if(response.total!! > 0){
            response.data?.let{
                binding.user = it[0]
            }
        }
    }

    private fun handleRvLoading(state: UiState) {
        when(state){
            is Loading -> displayRvLoadingState()
            is Success -> hideRvLoadingState()
            is Error -> displayRvErrorState(state.error)
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun setupViewListener() {
        binding.ivDiscrete.onClick {
            navigateToArticle()
        }
        binding.cvQrUser.onClick {
            showDialog()
        }
        binding.srHomeUser.onRefresh {
            viewModel.getUserData()
            viewModel.getRecentBooks()
        }
        rlSearchUser.onClick {
            navigateToSearch()
        }
    }

    private fun navigateToArticle() {
        start<ArticleActivity>{}
    }

    private fun navigateToSearch() {
        start<SearchBookActivity>{}
    }

    private fun displayRvErrorState(error: Throwable) {
        hideRvLoadingState()
        toast("${error.message}")
    }

    private fun hideRvLoadingState() {
        binding.pbRecentBooksUser.gone()
        binding.rvRecentBooksUser.visible()
    }

    private fun displayRvLoadingState() {
        binding.rvRecentBooksUser.gone()
        binding.pbRecentBooksUser.visible()
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.srHomeUser.isRefreshing = false
    }

    private fun displayLoadingState() {
        binding.srHomeUser.isRefreshing = true
    }

    private fun showDialog(){
        (activity as UserActivity).showQrDialog()
    }

//    private fun generateDiscreteViews() {
//        discreteList.add(
//            BannerModel(
//                getString(R.string.img_url_lib_2),
//                "Tata Cara Peminjaman Buku"
//            )
//        )
//        discreteList.add(
//            BannerModel(
//                getString(R.string.pic_linkcovid),
//                "Info Covid-19"
//            )
//        )
//        if(bannerRvAdapter.itemCount<1){
//            bannerRvAdapter.insertAndNotify(discreteList)
//        }
//    }
}
