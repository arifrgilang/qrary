package com.rz.qrary.presentation.user.booklist.userhistory

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.loan.MHistoryDetail
import com.rz.qrary.databinding.ActivityUserHistoryBinding
import com.rz.qrary.presentation.history.HistoryActivity
import com.express.domain.qrary.LIST_HISTORY
import com.express.domain.qrary.NPM
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.orhanobut.hawk.Hawk
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.start
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.other.CustomRvMargin
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import javax.inject.Inject

@AndroidEntryPoint
class UserHistoryActivity : BaseBindingActivity<ActivityUserHistoryBinding>() {
    private val viewModel: UserHistoryViewModel by viewModels()
    @Inject lateinit var rvAdapter: UserHistoryRvAdapter
    private val npm = Hawk.get<String>(NPM)

    override fun contentView(): Int = R.layout.activity_user_history

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupToolbar("HISTORY", true)
        initRv()
        setupViewListener()
        observeViewModel()
        viewModel.getHistory(npm)
    }

    private fun initRv() {
        with(binding.rvUserHistory){
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object: AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToDetailHistory(extra)
                            }
                        }
                    )
                }
            layoutManager = LinearLayoutManager(this@UserHistoryActivity)
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    this@UserHistoryActivity,
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }

    private fun navigateToDetailHistory(ticketId: String) {
        start<HistoryActivity>{
            putExtra("ticketId", ticketId)
            putExtra("historyType",
                LIST_HISTORY
            )
        }
    }

    private fun setupViewListener() {
        toolbar_back.onClick {
            finish()
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.listResponse, ::onResponse)
    }

    private fun onResponse(response: ResponseArray<MHistoryDetail>) {
        response.data?.let{
            with(rvAdapter){
                clearAndNotify()
                insertAndNotify(it)
                if(itemCount<1) showEmptyState()
            }
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showEmptyState(){
        binding.tvUserHistoryEmpty.visible()
        binding.rvUserHistory.gone()
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.srUserHistory.isRefreshing = false
        binding.rvUserHistory.visible()
        binding.tvUserHistoryEmpty.gone()
    }

    private fun showLoadingState() {
        binding.srUserHistory.isRefreshing = true
        binding.rvUserHistory.gone()
        binding.tvUserHistoryEmpty.gone()
    }
}
