package com.rz.qrary.presentation.splashscreen

import android.content.Intent
import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.rz.qrary.databinding.ActivitySplashBinding
import com.rz.qrary.presentation.onboarding.OnboardingActivity
import com.rz.qrary.util.ext.start

class SplashActivity : BaseBindingActivity<ActivitySplashBinding>() {
    override fun contentView(): Int = R.layout.activity_splash
    override fun setupData(savedInstanceState: Bundle?) {}
    override fun setupView() {
        start<OnboardingActivity>{}
        finish()
    }
}
