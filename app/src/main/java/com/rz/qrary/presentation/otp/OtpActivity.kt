package com.rz.qrary.presentation.otp

import `in`.aabhasjindal.otptextview.OTPListener
import android.app.Activity
import android.os.Bundle
import androidx.activity.viewModels
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.auth.MVerifyOTP
import com.rz.qrary.databinding.ActivityOtpBinding
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.other.AppUtils
import com.rz.qrary.util.state.*
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.longToast
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

@AndroidEntryPoint
class OtpActivity : BaseBindingActivity<ActivityOtpBinding>() {
    private val viewModel: OtpViewModel by viewModels()
    private var email: String? = ""
    private var otpMessage = ""

    override fun contentView(): Int = R.layout.activity_otp

    override fun setupData(savedInstanceState: Bundle?) {
        intent?.let{ email = it.getStringExtra("email") }
        sendOtp()
    }

    override fun setupView() {
        setupListener()
        observeViewModel()
    }

    private fun setupListener() {
        binding.btnResendOtp.onClick {
            sendOtp()
        }

        binding.tvOtp.otpListener = object: OTPListener{
            override fun onInteractionListener(){}
            override fun onOTPComplete(otp: String){
                viewModel.verifyOTP(
                    MVerifyOTP(
                        otp,
                        email
                    )
                )
            }
        }
    }

    private fun observeViewModel() {
        observe(viewModel.countdownTimer, ::setTimer)
        observe(viewModel.isCountdownFinished, ::handleCountdownFinished)
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.isOtpCorrect, ::onOtpChecked)
        observe(viewModel.otpMessage, ::onOtpCheckedMessage)
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.pbOtp.gone()
        binding.tvTimerOtp.visible()
        binding.btnResendOtp.visible()
    }

    private fun displayLoadingState() {
        binding.tvTimerOtp.gone()
        binding.btnResendOtp.gone()
        binding.pbOtp.visible()
    }

    private fun onOtpChecked(status: FormState) {
        when(status){
            is Invalid -> handleInvalidOtp()
            is Valid -> handleValidOtp()
        }
    }

    private fun handleValidOtp() {
        binding.tvOtp.showSuccess()
        longToast(otpMessage)
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun handleInvalidOtp() {
        binding.tvOtp.setOTP("")
        binding.tvOtp.showError()
        showBSD(R.drawable.bsd_failed, otpMessage)
    }

    private fun handleCountdownFinished(status: Boolean) {
        binding.isTimerFinished = status
    }

    private fun onOtpCheckedMessage(msg: String) {
        otpMessage = msg
    }

    private fun setTimer(time: String) {
        binding.timer = time
    }

    private fun sendOtp(){
        email?.let{ if (it.isNotEmpty()) viewModel.sendOTP(it) }
    }

    private fun showBSD(resId: Int, content: String) =
        AppUtils.showBSD(resId, content, supportFragmentManager)
}
