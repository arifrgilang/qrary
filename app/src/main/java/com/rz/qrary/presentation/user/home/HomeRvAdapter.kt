package com.rz.qrary.presentation.user.home

import android.content.Context
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.book.MBook
import com.rz.qrary.databinding.ItemBookBinding
import dagger.hilt.android.qualifiers.ActivityContext
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

class HomeRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MBook, ItemBookBinding, HomeRvAdapter.ViewHolder>(context){

    override fun getResLayout(type: Int): Int = R.layout.item_book

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemBookBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MBook) {
            view.book = model
            view.cvItemBook.onClick {
                getCallback()!!.onRecyclerItemClicked(model.id!!)
            }
        }
    }
}