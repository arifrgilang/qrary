package com.rz.qrary.presentation.user.home

data class BannerModel(
    val urlFoto: String?,
    val header: String?
)