package com.rz.qrary.presentation.admin.dashboard

import android.Manifest
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.PopupMenu
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.express.domain.qrary.SCAN_ADD_BUKU
import com.express.domain.qrary.SCAN_PEMINJAM
import com.express.domain.qrary.SCAN_PENGEMBALIAN
import com.express.domain.qrary.SCAN_VISITOR
import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.book.MBook
import com.express.domain.model.account.MUser
import com.rz.qrary.databinding.FragmentDashboardBinding
import com.rz.qrary.presentation.ScannerCallback
import com.rz.qrary.presentation.bookdetail.BookDetailActivity
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.presentation.LogoutCallback
import com.rz.qrary.presentation.searchbook.SearchBookActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.DexterBuilder
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener.Builder.withContext
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.rz.qrary.util.ext.*
import com.rz.qrary.util.other.CustomRvMargin
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.search_toolbar.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

@AndroidEntryPoint
class DashboardFragment : BaseBindingFragment<FragmentDashboardBinding>() {
    private val viewModel: DashboardFragmentViewModel by viewModels()
    @Inject lateinit var rvAdapter: DashboardRvAdapter
    private lateinit var scannerCallback: ScannerCallback
    private lateinit var logoutCallback: LogoutCallback

    override fun contentView(): Int = R.layout.fragment_dashboard

    override fun onAttach(context: Context) {
        scannerCallback = activity as ScannerCallback
        logoutCallback = activity as LogoutCallback
        super.onAttach(context)
    }

    override fun setupData(savedInstanceState: Bundle?) {
        viewModel.getAdminData()
    }

    override fun setupView() {
        initRv()
        setupViewListener()
        observeViewModel()
        viewModel.getRecentBooks()
    }

    private fun initRv() {
        with(binding.rvRecentBooksAdmin){
            layoutManager = GridLayoutManager(requireContext(), 2)
            isNestedScrollingEnabled = false
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object : AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToDetail(extra)
                            }
                        }
                    )
                }
            addItemDecoration(
                CustomRvMargin(
                    requireContext(),
                    16,
                    CustomRvMargin.GRID_2
                )
            )
        }
    }

    private fun navigateToDetail(bookId: String) {
        start<BookDetailActivity>{
            putExtra("bookId", bookId)
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.adminProfileResponse, ::onResponse)
        observe(viewModel.rvState, ::handleRvLoading)
        observe(viewModel.recentBooksResponse, ::onRvResponse)
    }

    private fun onRvResponse(response: ResponseArray<MBook>) {
        response.data?.let{
            rvAdapter.clearAndNotify()
            rvAdapter.insertAndNotify(it)
        }
    }

    private fun onResponse(response: ResponseArray<MUser>) {
        if(response.total!! > 0){
            response.data?.let{
                binding.admin = it[0]
            }
        }
    }

    private fun handleRvLoading(state: UiState) {
        when(state){
            is Loading -> displayRvLoadingState()
            is Success -> hideRvLoadingState()
            is Error -> displayRvErrorState(state.error)
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayRvErrorState(error: Throwable) {
        hideRvLoadingState()
        toast("${error.message}")
    }

    private fun hideRvLoadingState() {
        binding.pbRecentBooksAdmin.gone()
        binding.rvRecentBooksAdmin.visible()
    }

    private fun displayRvLoadingState() {
        binding.rvRecentBooksAdmin.gone()
        binding.pbRecentBooksAdmin.visible()
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.srDashboardAdmin.isRefreshing = false
    }

    private fun displayLoadingState() {
        binding.srDashboardAdmin.isRefreshing = true
    }

    private fun setupViewListener() {
        binding.rlAddBuku.onClick {
            checkCameraPermission(SCAN_ADD_BUKU)
        }
        binding.rlScanPengembalian.onClick {
            checkCameraPermission(SCAN_PENGEMBALIAN)
        }
        binding.rlScanPeminjam.onClick {
            checkCameraPermission(SCAN_PEMINJAM)
        }
        binding.rlScanPengunjung.onClick {
            checkCameraPermission(SCAN_VISITOR)
        }
        binding.srDashboardAdmin.onRefresh {
            viewModel.getAdminData()
            viewModel.getRecentBooks()
        }
        ivAdminMenu.onClick {
            logoutAdmin()
        }
        rlSearchAdmin.onClick {
            navigateToSearch()
        }
    }

    private fun navigateToSearch() {
        start<SearchBookActivity>{}
    }

    private fun logoutAdmin() {
        logoutCallback.showBSDConfirmationLogout()
    }

    private fun checkCameraPermission(cameraRequest: Int) {
        Dexter
            .withContext(requireContext())
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object: MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    when(cameraRequest){
                        SCAN_ADD_BUKU -> longToast("Scan Barcode buku yang akan ditambahkan")
                        SCAN_PENGEMBALIAN -> longToast("Scan QRCode user yang akan mengembalikan buku")
                        SCAN_PEMINJAM -> longToast("Scan QRCode user yang akan meminjam buku")
                        SCAN_VISITOR -> longToast("Scan QRCode user yang berkunjung")
                    }
                    scannerCallback.startScanner(cameraRequest)
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

            }).check()
    }
}
