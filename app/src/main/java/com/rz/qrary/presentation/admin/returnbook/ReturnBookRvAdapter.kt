package com.rz.qrary.presentation.admin.returnbook

import android.content.Context
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.book.MBook
import com.rz.qrary.databinding.ItemTicketBookBinding
import com.rz.qrary.util.ext.gone
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class ReturnBookRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MBook, ItemTicketBookBinding, ReturnBookRvAdapter.ViewHolder>(context){

    override fun getResLayout(type: Int): Int = R.layout.item_ticket_book

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemTicketBookBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MBook) {
            view.book = model
            view.btnTicketDeleteBook.gone()
        }
    }
}