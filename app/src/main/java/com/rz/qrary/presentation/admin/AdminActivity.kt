package com.rz.qrary.presentation.admin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.rz.qrary.R
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.ticket.MTempPinjamResponse
import com.express.domain.model.account.MUser
import com.express.domain.model.book.MBook
import com.express.domain.model.visitor.MVisitorResponse
import com.express.domain.qrary.*
import com.rz.qrary.databinding.ActivityAdminBinding
import com.rz.qrary.presentation.admin.addbook.AddBookActivity
import com.rz.qrary.presentation.admin.addbook.BSDAddBook
import com.rz.qrary.presentation.admin.dashboard.BSDScannerPeminjam
import com.rz.qrary.presentation.admin.dashboard.BSDScannerPengembalian
import com.rz.qrary.presentation.admin.dashboard.BSDScannerVisitor
import com.rz.qrary.presentation.admin.dashboard.DashboardFragment
import com.rz.qrary.presentation.admin.listborrower.ListBorrowerFragment
import com.rz.qrary.presentation.admin.returnbook.ReturnBookActivity
import com.rz.qrary.presentation.admin.visitor.VisitorFragment
import com.rz.qrary.presentation.ticket.TicketActivity
import com.rz.qrary.presentation.user.library.LibraryFragment
import com.rz.qrary.util.other.*
import com.rz.qrary.util.state.*
import com.rz.qrary.util.view.BSDConfirmation
import com.rz.qrary.presentation.LogoutCallback
import com.rz.qrary.presentation.ScannerCallback
import com.rz.qrary.presentation.login.LoginActivity
import com.orhanobut.hawk.Hawk
import com.rz.qrary.util.ext.*
import dagger.hilt.android.AndroidEntryPoint
import me.ydcool.lib.qrmodule.activity.QrScannerActivity

@AndroidEntryPoint
class AdminActivity : BaseBindingActivity<ActivityAdminBinding>(),
    ScannerCallback, LogoutCallback {
    private val viewModel: AdminViewModel by viewModels()
    private var mBackPressed: Long = 0
    private var isbn = ""

    override fun contentView(): Int = R.layout.activity_admin

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupViewListener()
        showDashboardFragment()
        observeViewModel()
    }

    private fun setupViewListener() {
        binding.bottomNavigationAdmin.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.adminBeranda -> showDashboardFragment()
                R.id.adminInfoBuku -> showBookInfoFragment()
                R.id.adminDaftar -> showListBorrowerFragment()
                R.id.adminPengunjung -> showVisitorFragment()
            }
            true
        }
    }

    private fun showDashboardFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flAdmin, DashboardFragment::class.java, null)
            .commit()
    }

    private fun showBookInfoFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flAdmin, LibraryFragment::class.java, null)
            .commit()
    }

    private fun showListBorrowerFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flAdmin, ListBorrowerFragment::class.java, null)
            .commit()
    }

    private fun showVisitorFragment(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flAdmin, VisitorFragment::class.java, null)
            .commit()
    }

    private fun observeViewModel() {
        observe(viewModel.scanNpmState, ::handleValidityNPMScan)
        observe(viewModel.visitorResponse, ::onVisitorResponse)
        observe(viewModel.peminjamResponse, ::onPeminjamResponse)
        observe(viewModel.pengembalianResponse, ::onPengembalianResponse)
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.searchResponse, ::onIsbnResponse)
    }

    private fun onIsbnResponse(response: ResponseArray<MBook>) {
        if(response.total != 0){
            toast("ISBN telah terdaftar!")
        } else {
            navigateToAddBook(isbn)
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun hideLoadingState() {
        binding.rlAdminLoading.gone()
        binding.rlMainAdmin.visible()
    }

    private fun displayLoadingState() {
        binding.rlMainAdmin.gone()
        binding.rlAdminLoading.visible()
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun handleValidityNPMScan(state: FormState){
        when(state){
            is Invalid -> showInvalidScan()
        }
    }

    private fun onVisitorResponse(response: ResponseObject<MVisitorResponse>) {
        response.data?.let{
            showBSDVisitor(it.user!!, it.waktuMasuk!!)
        }
    }

    private fun onPeminjamResponse(response: ResponseObject<MTempPinjamResponse>) {
        response.data?.let{
            if(response.total!! > 0){
                showBSDPeminjam(it)
            } else {
                toast(response.message!!)
            }
        }
    }

    private fun onPengembalianResponse(response: ResponseArray<MHistoryDetail>) {
        response.data?.let{
            Log.d("TOTAL", response.total.toString())
            if(response.total!! > 0){
                showBSDPengembalian(it[0])
            } else {
                toast(response.message!!)
            }
        }
    }

    private fun navigateToAddBook(isbn: String){
        startActivityForResult(
            Intent(
                this@AdminActivity, AddBookActivity::class.java
            ).apply { putExtra("isbn", isbn) },
            ADD_BUKU
        )
    }

    private fun navigateToReturnBook(ticketId: String) {
        start<ReturnBookActivity>{
            putExtra("ticketId", ticketId)
        }
    }

    private fun navigateToPinjam(user: MUser, ticketId: String){
        start<TicketActivity>{
            putExtra("pinjamUser", user)
            putExtra("pinjamTicketId", ticketId)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            SCAN_PENGEMBALIAN -> {
                if(resultCode == Activity.RESULT_OK){
                    val npm = data!!.extras!!.getString(QrScannerActivity.QR_RESULT_STR)!!
                    viewModel.checkValidityScan(SCAN_PENGEMBALIAN, npm)
                } else {
                    showBSD(R.drawable.bsd_warning, "Scan dibatalkan")
                }
            }
            SCAN_PEMINJAM -> {
                if(resultCode == Activity.RESULT_OK){
                    val npm = data!!.extras!!.getString(QrScannerActivity.QR_RESULT_STR)!!
                    viewModel.checkValidityScan(SCAN_PEMINJAM, npm)
                } else {
                    showBSD(R.drawable.bsd_warning, "Scan dibatalkan")
                }
            }
            SCAN_VISITOR -> {
                if(resultCode == Activity.RESULT_OK){
                    val npm = data!!.extras!!.getString(QrScannerActivity.QR_RESULT_STR)!!
                    viewModel.checkValidityScan(SCAN_VISITOR, npm)
                } else {
                    showBSD(R.drawable.bsd_warning, "Scan dibatalkan")
                }
            }
            SCAN_ADD_BUKU -> {
                if(resultCode == Activity.RESULT_OK){
                    isbn = data!!.extras!!.getString(QrScannerActivity.QR_RESULT_STR)!!
                    if(isbn.substring(0,5) == "qrary"){
                        toast("Scan barcode ISBN buku yang ingin ditambahkan!")
                    } else {
                        viewModel.checkIsbn(isbn)
                    }
                } else {
                    showBSD(R.drawable.bsd_warning, "Scan dibatalkan")
                }
            }
            ADD_BUKU -> {
                if(resultCode == Activity.RESULT_OK){
                    val urlFoto = data?.getStringExtra("urlFotoAdded")
                    showBSDAddBook(urlFoto!!)
                } else {
                    showBSD(R.drawable.bsd_warning, "Scan dibatalkan")
                }
            }
            REQUEST_DELETE_BOOK -> {
                if(resultCode == Activity.RESULT_OK){
                    showBookInfoFragment()
                }
            }
        }
    }

    override fun startScanner(requestCode: Int){
        startActivityForResult(
            Intent(
                this@AdminActivity, QrScannerActivity::class.java
            ), requestCode
        )
    }

    private fun showInvalidScan(){
        showBSD(R.drawable.bsd_warning, "QR Code tidak valid!")
    }

    override fun showBSD(resId: Int, content: String) =
        AppUtils.showBSD(resId, content, supportFragmentManager)

    private fun showBSDAddBook(urlFoto: String){
        BSDAddBook
            .newInstance(urlFoto, this, this)
            .show(supportFragmentManager, "BSDAddBook")
    }

    private fun showBSDVisitor(user: MUser, dateTime: String){
        val date = getFormattedDate(dateTime)
        val time = getFormattedTime(dateTime)

        BSDScannerVisitor
            .newInstance(user, date, time, this,
                SCAN_VISITOR
            )
            .show(supportFragmentManager, "BSDVisitor")
    }

    private fun showBSDPeminjam(response: MTempPinjamResponse) {
        BSDScannerPeminjam
            .newInstance(
                response.pengunjung!!,
                response.id!!,
                object: BSDScannerPeminjam.PeminjamCallback{
                    override fun startPeminjamActivity(user: MUser, ticketId: String) {
                        navigateToPinjam(user, ticketId)
                    }
                }
            )
            .show(supportFragmentManager, "BSDPeminjam")
    }

    private fun showBSDPengembalian(response: MHistoryDetail) {
        BSDScannerPengembalian
            .newInstance(
                response.user!!,
                response.ticketId!!,
                object: BSDScannerPengembalian.ReturnBookCallback{
                    override fun startReturnBookActivity(ticketId: String) {
                        navigateToReturnBook(ticketId)
                    }
                }
            )
            .show(supportFragmentManager, "BSDPengembalian")
    }

    override fun onBackPressed() {
        if(mBackPressed + BACK_PRESSED_INTERVAL > System.currentTimeMillis()){
            super.onBackPressed()
        } else{
            toast("Tekan tombol kembali 2 kali untuk keluar dari aplikasi")
        }
        mBackPressed = System.currentTimeMillis()
    }

    override fun showBSDConfirmationLogout() {
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin ingin keluar?")
            .yesButtonMessage("Ya")
            .noButtonMessage("BATAL")
            .callback(
                object : BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        logoutAdmin()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDLogoutAdmin")
    }

    private fun logoutAdmin(){
        Hawk.delete(USER_TOKEN)
        Hawk.put(IS_USER_LOGIN, "")
        start<LoginActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }
}

