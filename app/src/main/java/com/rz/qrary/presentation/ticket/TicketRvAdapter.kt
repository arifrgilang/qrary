package com.rz.qrary.presentation.ticket

import android.content.Context
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.book.MBook
import com.rz.qrary.databinding.ItemTicketBookBinding
import dagger.hilt.android.qualifiers.ActivityContext
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

class TicketRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MBook, ItemTicketBookBinding, TicketRvAdapter.ViewHolder>(context){

    private var deleteListener: OnClickDelete? = null

    override fun getResLayout(type: Int): Int = R.layout.item_ticket_book

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    fun setOnDeleteListener(listener: OnClickDelete){
        deleteListener = listener
    }

    inner class ViewHolder(
        view: ItemTicketBookBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MBook) {
            view.book = model
            view.btnTicketDeleteBook.onClick {
                deleteListener?.onBookDeleteConfirmation(model.isbn!!, model.urlCover!!)
            }
        }
    }

    interface OnClickDelete{
        fun onBookDeleteConfirmation(isbn: String, urlFoto: String)
    }
}