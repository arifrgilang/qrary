package com.rz.qrary.presentation.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.express.domain.qrary.IS_USER_LOGIN
import com.express.domain.qrary.NPM
import com.express.domain.qrary.OTP_REQUEST_CODE
import com.express.domain.qrary.USER_TOKEN
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.base.ResponseObject
import com.express.domain.model.auth.MTokenRole
import com.rz.qrary.databinding.ActivityLoginBinding
import com.rz.qrary.presentation.admin.AdminActivity
import com.rz.qrary.presentation.user.UserActivity
import com.rz.qrary.presentation.otp.OtpActivity
import com.rz.qrary.presentation.register.RegisterActivity
import com.rz.qrary.util.other.AppUtils.Companion.showErrorIfEmpty
import com.rz.qrary.util.ext.*
import com.rz.qrary.util.other.*
import com.rz.qrary.util.state.*
import com.rz.qrary.presentation.forgotpwd.ForgotPwdActivity
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

@AndroidEntryPoint
class LoginActivity : BaseBindingActivity<ActivityLoginBinding>() {
    private val viewModel: LoginViewModel by viewModels()
    private var isRegisterSuccess = false

    override fun contentView(): Int = R.layout.activity_login
    override fun setupData(savedInstanceState: Bundle?) {
        intent?.let{
            isRegisterSuccess = it
                .getBooleanExtra("isRegisterSuccess", false)
        }
    }

    override fun setupView() {
        checkRegisterCallback()
        setupListener()
        observeViewModel()
    }

    private fun checkRegisterCallback() {
        if(isRegisterSuccess){
            showBSD(R.drawable.bsd_warning, getString(R.string.login_registersuccess))
            isRegisterSuccess = false
        }
    }

    private fun setupListener() {
        binding.tvLoginLupaSandi.onClick {
            start<ForgotPwdActivity>{}
        }

        binding.btnLogin.onClick {
            viewModel.validateForm(
                binding.etLoginNPM.text.toString(),
                binding.etLoginPassword.text.toString()
            )
        }

        binding.tvLoginDaftar.onClick {
            start<RegisterActivity>{}
            finish()
        }
    }

    private fun observeViewModel() {
        observe(viewModel.formState, ::handleValidationForm)
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.loginResponse, ::onResponse)
    }

    private fun handleValidationForm(state: FormState) {
        when(state){
            is Invalid -> handleInvalidForm()
            is Valid ->
                viewModel.login(
                    binding.etLoginNPM.text.toString(),
                    binding.etLoginPassword.text.toString()
                )
        }
    }

    private fun handleLoading(state: UiState){
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.pbLogin.gone()
        binding.btnLogin.visible()
    }

    private fun displayLoadingState() {
        binding.btnLogin.gone()
        binding.pbLogin.visible()
    }

    private fun toOtpPage(email: String){
        startActivityForResult(
            Intent(this@LoginActivity, OtpActivity::class.java)
                .apply {
                    putExtra("email", email)
                },
            OTP_REQUEST_CODE
        )
    }

    private fun toDashboardUser() {
        start<UserActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }

    private fun toDashboardAdmin(){
        start<AdminActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }

    private fun showBSD(resId: Int, content: String) =
        AppUtils.showBSD(resId, content, supportFragmentManager)

    private fun onResponse(response: ResponseObject<MTokenRole>){
        response.data?.let{ authModel ->
            if(response.total!! > 0){
                saveCredentials(authModel)
                handleNavigation(authModel)
                toast(response.message.toString())
            } else {
                response.message?.let{ msg ->
                    checkIfEmailConfirmed(msg)
                }
            }
        }
    }

    private fun saveCredentials(auth: MTokenRole){
        Hawk.put(USER_TOKEN, auth.token)
        Hawk.put(IS_USER_LOGIN, auth.role)
        Hawk.put(NPM, binding.etLoginNPM.text.toString())
    }

    private fun handleNavigation(auth: MTokenRole){
        when(auth.role){
            "superadmin" -> toDashboardAdmin()
            "user" -> toDashboardUser()
            else -> {}
        }
    }

    private fun checkIfEmailConfirmed(msg: String){
        val strings = msg.split(" ")
        if(strings[2] == "email"){
            toOtpPage(strings[3]) // email parameter
        } else {
            showBSD(R.drawable.bsd_failed, msg)
        }
    }

    private fun handleInvalidForm() {
        showBSD(R.drawable.bsd_warning, getString(R.string.bsdwarning_requiredfield))
        showErrorIfEmpty(binding.etLoginNPM, getString(R.string.warn_npm12char))
        if(binding.etLoginNPM.text.toString().length in 1..11){
            binding.etLoginNPM.error = getString(R.string.warn_npm12char)
        }
        showErrorIfEmpty(binding.etLoginPassword, getString(R.string.warn_pwdlength))
        if(!AppUtils.isPasswordValid(binding.etLoginPassword.text.toString())){
            binding.etLoginPassword.error = getString(R.string.warn_pwdlength)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == OTP_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                binding.btnLogin.performClick()
            }
        }
    }
}
