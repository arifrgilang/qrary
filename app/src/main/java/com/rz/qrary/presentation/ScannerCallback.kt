package com.rz.qrary.presentation

interface ScannerCallback {
    fun startScanner(requestCode: Int)
    fun showBSD(res: Int, msg: String)
}