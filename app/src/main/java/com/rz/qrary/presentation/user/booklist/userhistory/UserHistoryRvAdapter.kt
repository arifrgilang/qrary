package com.rz.qrary.presentation.user.booklist.userhistory

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.loan.MHistoryDetail
import com.rz.qrary.databinding.ItemHistoryUserBinding
import com.rz.qrary.util.other.getCurrentDateStandard
import com.rz.qrary.util.other.getFormattedDate
import com.rz.qrary.util.other.getFormattedDateStandard
import dagger.hilt.android.qualifiers.ActivityContext
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UserHistoryRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MHistoryDetail, ItemHistoryUserBinding, UserHistoryRvAdapter.ViewHolder>(
    context
) {
    override fun getResLayout(type: Int): Int = R.layout.item_history_user

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemHistoryUserBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MHistoryDetail) {
            view.tanggalMeminjam =
                getFormattedDate(model.tanggalMeminjam!!)
            view.tanggalKembali =
                getFormattedDate(model.tanggalKembali!!)
            view.jumlahBuku = model.books!!.size.toString()
            getStatus(model).let{ status ->
                val color = ColorStateList
                    .valueOf(
                        Color.parseColor(
                            when(status){
                                "Selesai" -> "#3BB54A"
                                "Sedang Meminjam" -> "#083E77"
                                "Terlambat" -> "#C53741"
                                else -> "#FFFFFF"
                            }
                        )
                    )
                view.cvItemHistoryUser.setCardBackgroundColor(color)
                view.chipPeminjamStatus.text = status
                view.chipPeminjamStatus.chipBackgroundColor = color
                view.chipPeminjamStatus.setTextColor(Color.parseColor("#FFFFFF"))
            }
            view.cvItemHistoryUser.onClick {
                getCallback()?.onRecyclerItemClicked(model.ticketId!!)
            }
        }
    }

    private fun getStatus(model: MHistoryDetail): String{
        val todayDate =  SimpleDateFormat("MM/dd/yyyy")
            .parse(getCurrentDateStandard())!!
        val kembaliDate = SimpleDateFormat("MM/dd/yyyy")
            .parse(getFormattedDateStandard(model.tanggalKembali!!))!!
        val diff = TimeUnit.DAYS
            .convert((todayDate.time - kembaliDate.time), TimeUnit.MILLISECONDS)
        var result = ""
        if(model.isDikembalikan!!){
            result = "Selesai"
        } else {
            if (diff <= 0){
                result = "Sedang Meminjam"
            } else {
                result = "Terlambat"
            }
        }
        return result
    }
}