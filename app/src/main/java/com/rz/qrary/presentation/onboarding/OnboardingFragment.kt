package com.rz.qrary.presentation.onboarding

import android.os.Bundle

import com.rz.qrary.util.base.view.BaseBindingFragment
import com.rz.qrary.databinding.FragmentOnboarding1Binding

class OnboardingFragment(private val layoutRes: Int) : BaseBindingFragment<FragmentOnboarding1Binding>() {
    override fun contentView(): Int = layoutRes
    override fun setupData(savedInstanceState: Bundle?) {}
    override fun setupView() {}
    companion object {
        @JvmStatic
        fun newInstance(layoutRes: Int) =
            OnboardingFragment(layoutRes)
    }
}
