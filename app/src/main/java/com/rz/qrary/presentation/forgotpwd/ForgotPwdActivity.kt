package com.rz.qrary.presentation.forgotpwd

import android.os.Bundle
import androidx.activity.viewModels
import com.rz.qrary.R
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.auth.MEmail
import com.rz.qrary.databinding.ActivityForgotPwdBinding
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.state.*
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.longToast
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

@AndroidEntryPoint
class ForgotPwdActivity : BaseBindingActivity<ActivityForgotPwdBinding>() {
    private val viewModel: ForgotPwdViewModel by viewModels()

    override fun contentView(): Int = R.layout.activity_forgot_pwd

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupToolbar("LUPA PASSWORD", true)
        setupViewListener()
        observeViewModel()
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.formState, ::handleForm)
        observe(viewModel.forgotResponse, ::onResponse)
    }

    private fun setupViewListener() {
        binding.btnForgotPwd.onClick {
            viewModel.validateForm(binding.etForgotEmail.text.toString())
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.pbForgotPwd.gone()
        binding.btnForgotPwd.visible()
    }

    private fun displayLoadingState() {
        binding.btnForgotPwd.gone()
        binding.pbForgotPwd.visible()
    }

    private fun handleForm(state: FormState) {
        when(state){
            is Invalid -> handleInvalidForm()
            is Valid ->
                viewModel.requestSendEmail(
                    binding.etForgotEmail.text.toString()
                )
        }
    }

    private fun handleInvalidForm(){
        binding.etForgotEmail.error = "Isi dengan Email Unpad yang telah terdaftar!"
    }

    private fun onResponse(response: ResponseObject<MEmail>) {
        response.message?.let{
            longToast(it)
            when(it){
                "Email berhasil dikirim" -> {
                    finish()
                }
            }
        }
    }
}
