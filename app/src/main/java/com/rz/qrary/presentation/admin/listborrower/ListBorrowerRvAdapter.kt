package com.rz.qrary.presentation.admin.listborrower

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.loan.MPeminjam
import com.rz.qrary.databinding.ItemPeminjamBinding
import com.rz.qrary.util.other.getCurrentDateStandard
import com.rz.qrary.util.other.getFormattedDate
import com.rz.qrary.util.other.getFormattedDateStandard
import dagger.hilt.android.qualifiers.ActivityContext
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ListBorrowerRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MPeminjam, ItemPeminjamBinding, ListBorrowerRvAdapter.ViewHolder>(context) {

    override fun getResLayout(type: Int): Int = R.layout.item_peminjam

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemPeminjamBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MPeminjam) {
            view.tanggalMeminjam =
                getFormattedDate(model.tanggalMeminjam!!)
            view.tanggalKembali =
                getFormattedDate(model.tanggalKembali!!)
            view.jumlahBuku = model.isbnBuku!!.size.toString()
            view.user = model.user
            getStatus(model).let{ status ->
                val color = ColorStateList
                    .valueOf(
                        Color.parseColor(
                            when(status){
                                "Selesai" -> "#3BB54A"
                                "Sedang Meminjam" -> "#083E77"
                                "Terlambat" -> "#C53741"
                                else -> "#FFFFFF"
                            }
                        )
                    )
                view.cvItemPengunjung.setCardBackgroundColor(color)
                view.chipPeminjamStatus.text = status
                view.chipPeminjamStatus.chipBackgroundColor = color
                view.chipPeminjamStatus.setTextColor(Color.parseColor("#FFFFFF"))
            }
            view.cvItemPengunjung.onClick {
                getCallback()?.onRecyclerItemClicked(model.idTicket!!)
            }
        }
    }

    private fun getStatus(model: MPeminjam): String{
        val todayDate =  SimpleDateFormat("MM/dd/yyyy")
            .parse(getCurrentDateStandard())!!
        val kembaliDate = SimpleDateFormat("MM/dd/yyyy")
            .parse(getFormattedDateStandard(model.tanggalKembali!!))!!
        val diff = TimeUnit.DAYS
            .convert((todayDate.time - kembaliDate.time), TimeUnit.MILLISECONDS)
        var result = ""
        if(model.isDikembalikan!!){
            result = "Selesai"
        } else {
            if (diff <= 0){
                result = "Sedang Meminjam"
            } else {
                result = "Terlambat"
            }
        }
        return result
    }
}