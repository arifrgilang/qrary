package com.rz.qrary.presentation.register

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseObject
import com.express.domain.model.account.MStudentData
import com.express.domain.model.auth.MRegister
import com.express.domain.model.account.MUser
import com.express.domain.model.auth.MLogin
import com.express.domain.model.auth.MTokenRole
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.AppUtils
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*

class RegisterViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){

    private val _npmState = MutableLiveData<FormState>()
    val npmState: LiveData<FormState>
        get() = _npmState

    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _registerResponse = MutableLiveData<ResponseObject<MUser>>()
    val registerResponse: LiveData<ResponseObject<MUser>>
        get() = _registerResponse

    private val _npmResponse = MutableLiveData<ResponseObject<MStudentData>>()
    val npmResponse: LiveData<ResponseObject<MStudentData>>
        get() = _npmResponse

    private val _loginResponse = MutableLiveData<ResponseObject<MTokenRole>>()
    val loginResponse: LiveData<ResponseObject<MTokenRole>>
        get() = _loginResponse

    fun login(npm: String, pwd: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .login(MLogin(npm, pwd))
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    { response ->
                        _loginResponse.postValue(response)
                        _uiState.value = Success
                    }, {
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun register(
        nama: String,
        npm: String,
        email: String,
        pwd: String,
        confirmPwd: String
    ){
        addToDisposable(
            repository.register(
                MRegister(
                    nama,
                    npm,
                    email,
                    pwd,
                    confirmPwd
                )
            )
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    { response ->
                        _registerResponse.postValue(response)
                        _uiState.value = Success
                    }, {
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun checkNPM(npm: String) {
        addToDisposable(
            repository.checkNpmAvailability(npm)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    { response ->
                        _npmResponse.postValue(response)
                        _uiState.value = Success
                    }, {
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun validateNPM(npm: String) {
        _uiState.value = Loading
        if(npm.isNotEmpty()){
            _npmState.value = Valid
        } else {
            _npmState.value = Invalid
            _uiState.value = Success
        }
    }

    fun validateForm(
        email: String,
        pwd: String,
        confirmPwd: String
    ) {
        _uiState.value = Loading
        if(
            email.isNotEmpty() &&
            pwd.isNotEmpty() &&
            confirmPwd.isNotEmpty() &&
            AppUtils.isPasswordValid(pwd) &&
            AppUtils.isEmailValid(email) &&
            confirmPwd == pwd
        ){
            _formState.value = Valid
        } else {
            _formState.value = Invalid
            _uiState.value = Success
        }
    }
}