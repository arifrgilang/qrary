package com.rz.qrary.presentation.user.booklist.userhistory

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState

class UserHistoryViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _listResponse = MutableLiveData<ResponseArray<MHistoryDetail>>()
    val listResponse: LiveData<ResponseArray<MHistoryDetail>>
        get() = _listResponse

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    fun getHistory(npm: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getSpesificHistoryPeminjaman("npm", npm)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _listResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}