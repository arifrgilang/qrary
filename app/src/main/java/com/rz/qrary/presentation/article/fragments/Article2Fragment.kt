package com.rz.qrary.presentation.article.fragments

import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.rz.qrary.databinding.FragmentArticle2Binding

class Article2Fragment : BaseBindingFragment<FragmentArticle2Binding>() {

    companion object {
        @JvmStatic
        fun newInstance() = Article2Fragment()
    }

    override fun contentView(): Int = R.layout.fragment_article2

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {}
}