package com.rz.qrary.presentation.user.booklist.dipinjam

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.loan.MBukuDipinjam
import com.rz.qrary.databinding.ItemDipinjamBinding
import dagger.hilt.android.qualifiers.ActivityContext
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

class BookListRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MBukuDipinjam, ItemDipinjamBinding, BookListRvAdapter.ViewHolder>(context) {

    override fun getResLayout(type: Int): Int = R.layout.item_dipinjam

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemDipinjamBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MBukuDipinjam) {
            val color = ColorStateList.valueOf(
                Color.parseColor(
                    when(model.status){
                        "Sedang Meminjam" -> "#083E77"
                        "Terlambat" -> "#C53741"
                        else -> "#FFFFFF"
                    }
                )
            )
            view.detailDipinjam = model
            with(view.cvItemDipinjam){
                setCardBackgroundColor(color)
                onClick {
                    model.buku?.let{ book ->
                        getCallback()?.onRecyclerItemClicked(book.id!!)
                    }
                }
            }
            with(view.chipDipinjam){
                text = model.status
                chipBackgroundColor = color
                setTextColor(Color.parseColor("#FFFFFF"))
            }
        }
    }
}