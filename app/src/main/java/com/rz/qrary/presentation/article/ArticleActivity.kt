package com.rz.qrary.presentation.article

import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.rz.qrary.databinding.ActivityArticleBinding
import com.rz.qrary.util.other.ViewPagerAdapter
import com.rz.qrary.presentation.article.fragments.Article1Fragment
import com.rz.qrary.presentation.article.fragments.Article2Fragment
import com.rz.qrary.presentation.article.fragments.Article3Fragment
import com.rz.qrary.presentation.article.fragments.Article4Fragment

class ArticleActivity : BaseBindingActivity<ActivityArticleBinding>() {
    private lateinit var vpAdapter: ViewPagerAdapter
    override fun contentView(): Int = R.layout.activity_article

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupToolbar("TATA CARA PEMINJAMAN BUKU", true)
        setupVp()
    }

    private fun setupVp() {
        vpAdapter = ViewPagerAdapter(supportFragmentManager)
        with(vpAdapter){
            addFragment(Article1Fragment.newInstance(), "")
            addFragment(Article2Fragment.newInstance(), "")
            addFragment(Article3Fragment.newInstance(), "")
            addFragment(Article4Fragment.newInstance(), "")
        }
        with(binding.vpArticle){
            offscreenPageLimit = 1
            adapter = vpAdapter
        }
        binding.dotsArticle.setViewPager(binding.vpArticle)
    }
}
