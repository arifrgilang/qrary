package com.rz.qrary.presentation.admin.listborrower

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.model.loan.MPeminjam
import com.express.domain.qrary.Repository
import com.express.domain.qrary.LIST_BORROWER
import com.express.domain.qrary.LIST_HISTORY
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState

class ListBorrowerViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _listResponse = MutableLiveData<ResponseArray<MPeminjam>>()
    val listResponse: LiveData<ResponseArray<MPeminjam>>
        get() = _listResponse

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    fun getList(type: String){
        when(type){
            LIST_BORROWER -> getBorrower()
            LIST_HISTORY -> getHistory()
        }
    }

    private fun getBorrower() {
        _uiState.value = Loading
        addToDisposable(
            repository
                .getAllBorrower()
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _listResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    private fun getHistory(){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getAllHistory()
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _listResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}