package com.rz.qrary.presentation.onboarding

import android.content.Intent
import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.rz.qrary.databinding.ActivityOnboardingBinding
import com.rz.qrary.presentation.admin.AdminActivity
import com.rz.qrary.presentation.user.UserActivity
import com.rz.qrary.presentation.login.LoginActivity
import com.rz.qrary.presentation.register.RegisterActivity
import com.express.domain.qrary.IS_USER_LOGIN
import com.rz.qrary.util.other.ViewPagerAdapter
import com.orhanobut.hawk.Hawk
import com.rz.qrary.util.ext.start
import org.jetbrains.anko.sdk27.coroutines.onClick

class OnboardingActivity : BaseBindingActivity<ActivityOnboardingBinding>() {
    override fun contentView(): Int = R.layout.activity_onboarding
    override fun setupData(savedInstanceState: Bundle?) {
        checkCredentials()
        setupViewPager()
    }

    override fun setupView() {
        binding.btnOnboardingDaftar.onClick { start<RegisterActivity>{} }
        binding.tvOnboardingMasuk.onClick{ start<LoginActivity>{} }
    }

    private fun getCredentials(): String? = Hawk.get(IS_USER_LOGIN)

    private fun checkCredentials() {
        when(getCredentials()){
            "superadmin" -> navigateToAdminDashboard()
            "user" -> navigateToUserDashboard()
            else -> {}
        }
    }

    private fun setupViewPager(){
        with(binding.vpOnboarding){
            adapter = getVpAdapter()
            binding.dotsOnboarding.setViewPager(this)
            offscreenPageLimit = 1
        }
    }

    private fun getVpAdapter() = ViewPagerAdapter(supportFragmentManager)
        .apply {
            addFragment(OnboardingFragment.newInstance(R.layout.fragment_onboarding1), "")
            addFragment(OnboardingFragment.newInstance(R.layout.fragment_onboarding2), "")
            addFragment(OnboardingFragment.newInstance(R.layout.fragment_onboarding3), "")
        }

    private fun navigateToUserDashboard() {
        start<UserActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }

    private fun navigateToAdminDashboard() {
        start<AdminActivity>{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }
}
