package com.rz.qrary.presentation

interface LogoutCallback {
    fun showBSDConfirmationLogout()
}