package com.rz.qrary.presentation.admin.visitor

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.model.visitor.MVisitorResponse
import com.express.domain.qrary.Repository
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState

class VisitorViewModel @ViewModelInject constructor(
    private val repository: Repository
): BaseRxViewModel() {
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _visitorResponse = MutableLiveData<ResponseArray<MVisitorResponse>>()
    val visitorResponse: LiveData<ResponseArray<MVisitorResponse>>
        get() = _visitorResponse

    fun getVisitor(){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getVisitor()
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _visitorResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}