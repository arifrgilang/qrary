package com.rz.qrary.presentation.user.booklist.dipinjam

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager

import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.loan.MBukuDipinjam
import com.rz.qrary.databinding.FragmentBookListBinding
import com.rz.qrary.presentation.bookdetail.BookDetailActivity
import com.rz.qrary.presentation.user.booklist.userhistory.UserHistoryActivity
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.start
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.other.CustomRvMargin
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

@AndroidEntryPoint
class BookListFragment : BaseBindingFragment<FragmentBookListBinding>() {
    private val viewModel: BookListViewModel by viewModels()
    @Inject lateinit var rvAdapter: BookListRvAdapter

    override fun contentView(): Int = R.layout.fragment_book_list

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        initRv()
        setupViewListener()
        observeViewModel()
        viewModel.getDipinjamBooks()
    }

    private fun initRv() {
        with(binding.rvDipinjam){
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object : AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToBookDetail(extra)
                            }
                        }
                    )
                }
            layoutManager = LinearLayoutManager( requireContext() )
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    requireContext(),
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }

    private fun setupViewListener() {
        binding.ivDipinjamHistory.onClick {
            start<UserHistoryActivity>{}
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.listResponse, ::onResponse)
    }

    private fun onResponse(response: List<MBukuDipinjam>) {
        with(rvAdapter){
            clearAndNotify()
            insertAndNotify(response)
            if(itemCount<1) showEmptyState()
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun showEmptyState() {
        binding.tvDipinjamEmpty.visible()
        binding.rvDipinjam.gone()
    }

    private fun hideLoadingState() {
        binding.srDipinjam.isRefreshing = false
        binding.rvDipinjam.visible()
        binding.tvDipinjamEmpty.gone()
    }

    private fun showLoadingState() {
        binding.srDipinjam.isRefreshing = true
        binding.rvDipinjam.gone()
        binding.tvDipinjamEmpty.gone()
    }

    private fun navigateToBookDetail(bookId: String) {
        start<BookDetailActivity>{
            putExtra("bookId", bookId)
        }
    }
}
