package com.rz.qrary.presentation.admin.listborrower

import android.os.Bundle
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.loan.MPeminjam
import com.rz.qrary.databinding.FragmentListBorrowerBinding
import com.rz.qrary.presentation.history.HistoryActivity
import com.rz.qrary.util.ext.gone
import com.express.domain.qrary.LIST_BORROWER
import com.express.domain.qrary.LIST_HISTORY
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.start
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.other.CustomRvMargin
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

@AndroidEntryPoint
class ListBorrowerFragment : BaseBindingFragment<FragmentListBorrowerBinding>() {
    private val viewModel: ListBorrowerViewModel by viewModels()
    @Inject lateinit var rvAdapter: ListBorrowerRvAdapter
    private lateinit var listType: String

    override fun contentView(): Int = R.layout.fragment_list_borrower

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        initRv()
        setupViewListener()
        observeViewModel()
        getListType(LIST_BORROWER)
    }

    private fun setupViewListener() {
        binding.srListBorrower.onRefresh {
            getListType(listType)
        }
        binding.ivBorrowerMenu.onClick {
            showMenu()
        }
    }

    private fun showMenu() {
        PopupMenu(requireContext(), binding.ivBorrowerMenu)
            .apply {
                menuInflater.inflate(R.menu.borrower_menu, menu)
                setOnMenuItemClickListener {
                    when(it.itemId){
                        R.id.borrowerMenuPeminjam -> getListType(LIST_BORROWER)
                        R.id.borrowerMenuHistory -> getListType(LIST_HISTORY)
                    }
                    true
                }
                show()
            }
    }

    private fun getListType(type: String){
        listType = type
        binding.tvBorrowerToolbar.text = listType
        viewModel.getList(listType)
    }

    private fun initRv() {
        with(binding.rvPeminjam){
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object: AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToHistoryDetail(extra)
                            }
                        }
                    )
                }
            layoutManager = LinearLayoutManager(requireContext())
                .apply {
                    reverseLayout = true
                    stackFromEnd = true
                }
            addItemDecoration(
                CustomRvMargin(
                    requireContext(),
                    16,
                    CustomRvMargin.LINEAR_VERTICAL_REVERSED
                )
            )
        }
    }

    private fun navigateToHistoryDetail(ticketId: String) {
        start<HistoryActivity>{
            putExtra("ticketId", ticketId)
            putExtra("historyType", listType)
        }
    }

    private fun observeViewModel() {
        observe(viewModel.listResponse, ::onResponse)
        observe(viewModel.uiState, ::handleLoading)
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.srListBorrower.isRefreshing = false
        binding.rvPeminjam.visible()
    }

    private fun showLoadingState() {
        binding.srListBorrower.isRefreshing = true
        binding.rvPeminjam.gone()
    }

    private fun onResponse(response: ResponseArray<MPeminjam>) {
        with(rvAdapter){
            clearAndNotify()
            insertAndNotify(response.data)
        }
    }
}
