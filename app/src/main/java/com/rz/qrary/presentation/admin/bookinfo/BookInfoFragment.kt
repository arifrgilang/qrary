package com.rz.qrary.presentation.admin.bookinfo

import android.os.Bundle

import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.rz.qrary.databinding.FragmentBookInfoBinding

class BookInfoFragment : BaseBindingFragment<FragmentBookInfoBinding>() {
    override fun contentView(): Int = R.layout.fragment_book_info

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {}
}
