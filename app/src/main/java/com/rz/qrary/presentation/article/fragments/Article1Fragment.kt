package com.rz.qrary.presentation.article.fragments

import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.rz.qrary.databinding.FragmentArticle1Binding

class Article1Fragment : BaseBindingFragment<FragmentArticle1Binding>() {

    companion object {
        @JvmStatic
        fun newInstance() = Article1Fragment()
    }

    override fun contentView(): Int = R.layout.fragment_article1

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {}
}
