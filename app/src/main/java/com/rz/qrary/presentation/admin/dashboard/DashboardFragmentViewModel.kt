package com.rz.qrary.presentation.admin.dashboard

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.model.book.MBook
import com.express.domain.model.account.MUser
import com.express.domain.qrary.NPM
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.orhanobut.hawk.Hawk

class DashboardFragmentViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _adminProfileResponse = MutableLiveData<ResponseArray<MUser>>()
    val adminProfileResponse: LiveData<ResponseArray<MUser>>
        get() = _adminProfileResponse

    private val _rvState = MutableLiveData<UiState>()
    val rvState: LiveData<UiState>
        get() = _rvState

    private val _recentBooksResponse = MutableLiveData<ResponseArray<MBook>>()
    val recentBooksResponse: LiveData<ResponseArray<MBook>>
        get() = _recentBooksResponse

    fun getAdminData(){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getUser("npm", Hawk.get(NPM))
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _adminProfileResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun getRecentBooks(){
        _rvState.value = Loading
        addToDisposable(
            repository
                .getRecentBooks()
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _recentBooksResponse.value = it
                        _rvState.value = Success
                    },{
                        _rvState.value = Error(it)
                    }
                )
        )
    }
}