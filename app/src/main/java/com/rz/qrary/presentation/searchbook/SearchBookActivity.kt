package com.rz.qrary.presentation.searchbook

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.book.MBook
import com.rz.qrary.databinding.ActivitySearchBookBinding
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.presentation.bookdetail.BookDetailActivity
import com.rz.qrary.util.ext.*
import com.rz.qrary.util.other.CustomRvMargin
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

@AndroidEntryPoint
class SearchBookActivity : BaseBindingActivity<ActivitySearchBookBinding>() {
    private val viewModel: SearchBookViewModel by viewModels()
    @Inject lateinit var rvAdapter: SearchBookRvAdapter
    override fun contentView(): Int = R.layout.activity_search_book

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        initRv()
        setViewListener()
        observeViewModel()
        viewModel.searchBook("")
    }

    private fun initRv() {
        with(binding.rvSearchBook){
            layoutManager = GridLayoutManager(this@SearchBookActivity, 2)
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object : AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToDetail(extra)
                            }
                        }
                    )
                }
            addItemDecoration(
                CustomRvMargin(
                    this@SearchBookActivity,
                    16,
                    CustomRvMargin.GRID_2
                )
            )
        }
    }

    private fun navigateToDetail(bookId: String) {
        start<BookDetailActivity>{
            putExtra("bookId", bookId)
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.searchResponse, ::onResponse)
    }

    private fun showKeyboard(et: EditText){
        et.requestFocus()
        val imm = et
            .context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    private fun hideSoftKeyboard(et: EditText) {
        et.clearFocus()
        val imm = et
            .context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(et.windowToken, 0)
    }

    private fun setViewListener() {
        with(binding.etSearch){
            post { // to handle auto focus to edittext
                 showKeyboard(this)
            }
            addTextChangedListener(
                object: TextWatcher{
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        binding.ivClearSearch.visible()
                        if(s!!.isEmpty()){
                            binding.ivClearSearch.gone()
                            viewModel.searchBook("")
                        }
                    }
                    override fun afterTextChanged(s: Editable?) {}
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {}
                }
            )
            setOnEditorActionListener{ _, actionId, _ ->
                return@setOnEditorActionListener when (actionId){
                    EditorInfo.IME_ACTION_SEARCH -> {
                        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                            .apply {
                                hideSoftInputFromWindow(
                                    currentFocus!!.windowToken,
                                    InputMethodManager.HIDE_NOT_ALWAYS
                                )
                            }
                        viewModel.searchBook(binding.etSearch.text.toString())
                        hideSoftKeyboard(binding.etSearch)
                        true
                    }
                    else -> false
                }
            }
        }

        binding.ivClearSearch.onClick {
            binding.etSearch.setText("")
            showKeyboard(binding.etSearch)
            viewModel.searchBook("")
        }

        binding.ivBackSearch.onClick {
            hideSoftKeyboard(binding.etSearch)
            finish()
        }
    }

    private fun onResponse(response: ResponseArray<MBook>){
        response.data?.let{
            with(rvAdapter){
                clearAndNotify()
                insertAndNotify(it)
            }
        }
        if(response.total!! < 1) showEmptyState()
    }

    private fun showEmptyState() {
        binding.rvSearchBook.gone()
        binding.tvEmptySearch.visible()
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.pbSearchBook.gone()
        binding.rvSearchBook.visible()
    }

    private fun showLoadingState() {
        binding.rvSearchBook.gone()
        binding.tvEmptySearch.gone()
        binding.pbSearchBook.visible()
    }
}
