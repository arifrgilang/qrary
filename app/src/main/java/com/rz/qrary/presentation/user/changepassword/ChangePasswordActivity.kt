package com.rz.qrary.presentation.user.changepassword

import android.os.Bundle
import androidx.activity.viewModels
import com.rz.qrary.R
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.account.MUser
import com.rz.qrary.databinding.ActivityChangePasswordBinding
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.ext.visible
import com.rz.qrary.util.state.*
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

@AndroidEntryPoint
class ChangePasswordActivity : BaseBindingActivity<ActivityChangePasswordBinding>() {
    private val viewModel: ChangePasswordViewModel by viewModels()
    override fun contentView(): Int = R.layout.activity_change_password

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupToolbar("UBAH KATA SANDI", true)
        setupViewListener()
        observeViewModel()
    }

    private fun setupViewListener() {
        binding.btnChangePwd.onClick {
            viewModel.validateForm(
                binding.etOldPwd.text.toString(),
                binding.etNewPwd.text.toString(),
                binding.etConfNewPwd.text.toString()
            )
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.formState, ::handleForm)
        observe(viewModel.changeResponse, ::onResponse)
    }

    private fun onResponse(response: ResponseObject<MUser>) {
        response.message?.let{
            toast(it)
            when(it){
                "Kata sandi berhasil diperbarui" -> finish()
            }
        }
    }

    private fun handleForm(state: FormState) {
        when(state){
            is Invalid -> handleInvalidForm()
            is Valid ->
                viewModel.changePassword(
                    binding.etOldPwd.text.toString(),
                    binding.etNewPwd.text.toString()
                )
        }
    }

    private fun handleInvalidForm() {
        if(binding.etOldPwd.text.toString().isEmpty()){
            binding.etOldPwd.error = "Pastikan kata sandi lama Anda benar!"
        }
        if(binding.etNewPwd.text.toString().isEmpty()){
            binding.etNewPwd.error = "Pastikan kata sandi baru terisi!"
        }
        if(binding.etConfNewPwd.text.toString().isEmpty()){
            binding.etConfNewPwd.error = "Pastikan konfirmasi kata sandi baru terisi!"
        }
        if(binding.etNewPwd.text.toString() != binding.etConfNewPwd.text.toString()){
            binding.etConfNewPwd.error = "Pastikan konfirmasi kata sandi sama"
        }
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.pbChangePwd.gone()
        binding.btnChangePwd.visible()
    }

    private fun displayLoadingState() {
        binding.btnChangePwd.gone()
        binding.pbChangePwd.visible()
    }
}
