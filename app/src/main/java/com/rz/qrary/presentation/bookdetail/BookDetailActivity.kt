package com.rz.qrary.presentation.bookdetail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.rz.qrary.R
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.express.domain.model.book.MBook
import com.express.domain.model.book.MDeleteBook
import com.rz.qrary.databinding.ActivityBookDetailBinding
import com.rz.qrary.util.ext.observe
import com.express.domain.qrary.IS_USER_LOGIN
import com.express.domain.qrary.REQUEST_EDIT_BOOK
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.util.view.BSDConfirmation
import com.rz.qrary.presentation.admin.editbook.EditBookActivity
import com.google.android.material.chip.Chip
import com.orhanobut.hawk.Hawk
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.visible
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

@AndroidEntryPoint
class BookDetailActivity : BaseBindingActivity<ActivityBookDetailBinding>() {
    private val viewModel: BookDetailViewModel by viewModels()
    private var id = ""
    private lateinit var role: String
    private lateinit var book: MBook

    override fun contentView(): Int = R.layout.activity_book_detail

    override fun setupData(savedInstanceState: Bundle?) {
        intent?.let {
            id = it.getStringExtra("bookId")!!
        }
        role = Hawk.get(IS_USER_LOGIN)
        viewModel.getBookDetail(id)
    }

    override fun setupView() {
        setupToolbar("Detail Buku", true)
        setupRole()
        setupViewListener()
        observeViewModel()
    }

    private fun setupViewListener() {
        binding.btnDeleteBook.onClick {
            showConfirmationDelete()
        }
        binding.btnEditBook.onClick {
            navigateToEditBook()
        }
    }

    private fun navigateToEditBook() {
        startActivityForResult(
            Intent(this, EditBookActivity::class.java).apply {
                putExtra("book", book)
            },
            REQUEST_EDIT_BOOK
        )
    }

    private fun setupRole() {
        when(role){
            "superadmin" -> showEditableBook(true)
            "user" -> showEditableBook(false)
        }
    }

    private fun showEditableBook(isAdmin: Boolean) {
        if(isAdmin) binding.llEditable.visible() else binding.llEditable.gone()
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.bookResponse, ::onResponse)
        observe(viewModel.deleteResponse, ::onDelete)
    }

    private fun onDelete(response: ResponseObject<MDeleteBook>) {
        response.message?.let{ message ->
            when(message){
                "Buku berhasil dihapus" -> {
                    toast(message)
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                else -> toast(message)
            }
        }
    }

    private fun onResponse(response: ResponseObject<MBook>) {
        response.message?.let{
            if(it == "Buku tidak ditemukan"){
                toast(it)
                finish()
            } else {
                response.data?.let{
                    book = it
                    binding.book = it
                    binding.ivDetailBgCover.setBlur(20)
                    setCategory(it.kategori!!)
                }
            }
        }
    }

    private fun setCategory(list: List<String>) {
        for(x in list.indices){
            addChip(list[x])
        }
    }

    private fun addChip(category: String) {
        binding.cgDetailCategory.addView(
            Chip(this)
                .apply { text = category })
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
        finish()
    }

    private fun hideLoadingState() {
        binding.rlDetailBukuLoading.gone()
        binding.svDetailBuku.visible()
        setupRole()
    }

    private fun showLoadingState() {
        binding.svDetailBuku.gone()
        binding.rlDetailBukuLoading.visible()
        showEditableBook(false)
    }

    private fun showConfirmationDelete() {
        BSDConfirmation.Builder
            .urlFoto(book.urlCover!!)
            .yesButtonMessage("Ya, hapus buku ini")
            .noButtonMessage("BATAL")
            .headerMessage("Yakin ingin menghapus buku ini?")
            .callback(
                object: BSDConfirmation.OnConfirmationCallback{
                    override fun onUserAgreed() {
                        viewModel.deleteBook(book.id!!)
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "Confirmation Delete")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                REQUEST_EDIT_BOOK -> {
                    binding.cgDetailCategory.removeAllViews()
                    viewModel.getBookDetail(id)
                }
            }
        }
    }
}
