package com.rz.qrary.presentation.ticket

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import kotlinx.android.synthetic.main.bsd_date.*
import kotlinx.android.synthetic.main.bsd_date.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.toast
import java.text.SimpleDateFormat
import java.util.*

class BSDDate(
    private val callback: BSDDateCallback
) : BottomSheetDialogFragment(){
    companion object{
        @JvmStatic
        fun newInstance(
            callback: BSDDateCallback
        ) = BSDDate(callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bsd_date, container, false)
        view.etTicketDateFrom.setText(getCurrentDate())
        view.etTicketDateTo.onClick{
            // handling initial date
            val date = getInitialDate()
            val day = date[1].toInt()
            val month = date[0].toInt() - 1
            val year = date[2].toInt()

            SpinnerDatePickerDialogBuilder()
                .context(requireContext())
                .spinnerTheme(R.style.NumberPickerStyle)
                .showDaySpinner(true)
                .defaultDate(year, month, day)
                .callback { _, year, monthOfYear, dayOfMonth ->
                    view.etTicketDateTo.setText(
                        SimpleDateFormat("MM/dd/yyyy")
                            .format(Date(year-1900,monthOfYear,dayOfMonth))
                    )
                }
                .build()
                .show()
        }

        view.btnTicketAddDate.onClick{
            if(
                view.etTicketDateFrom.text.toString().isNotEmpty() &&
                view.etTicketDateTo.text.toString().isNotEmpty())
            {
                callback.onGetDate(
                    view.etTicketDateFrom.text.toString(),
                    view.etTicketDateTo.text.toString()
                )
            } else {
                toast("Tanggal tidak boleh kosong!")
            }
        }
        return view
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentDate(): String =
        SimpleDateFormat("MM/dd/YYYY")
            .format(Calendar.getInstance().time)

    private fun getInitialDate(): List<String>{
        val currentDate = getCurrentDate().split("/")
        val isEmpty = etTicketDateTo.text.toString().isEmpty()
        val splitedDate = if(isEmpty) arrayListOf("","","") else etTicketDateTo.text.toString().split("/")
        return if(isEmpty) currentDate else splitedDate
    }
    interface BSDDateCallback{
        fun onGetDate(dateFrom: String, dateTo: String)
    }
}