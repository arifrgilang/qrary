package com.rz.qrary.presentation.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseObject
import com.express.domain.model.auth.MLogin
import com.express.domain.model.auth.MTokenRole
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.AppUtils
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*

class LoginViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel() {

    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _loginResponse = MutableLiveData<ResponseObject<MTokenRole>>()
    val loginResponse: LiveData<ResponseObject<MTokenRole>>
        get() = _loginResponse

    fun validateForm(npm: String, pwd: String){
        _uiState.value = Loading
        if(
            npm.isNotEmpty() &&
            pwd.isNotEmpty() &&
            npm.length >= 12 &&
            AppUtils.isPasswordValid(pwd)
        ) {
            _formState.value = Valid
        } else {
            _formState.value = Invalid
            _uiState.value = Success
        }
    }

    fun login(npm: String, pwd: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .login(MLogin(npm, pwd))
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    { response ->
                        _loginResponse.postValue(response)
                        _uiState.value = Success
                    }, {
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}