package com.rz.qrary.presentation.admin

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.express.domain.model.book.MBook
import com.express.domain.qrary.SCAN_PEMINJAM
import com.express.domain.qrary.SCAN_PENGEMBALIAN
import com.express.domain.qrary.SCAN_VISITOR
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.model.ticket.MTempPinjam
import com.express.domain.model.ticket.MTempPinjamResponse
import com.express.domain.model.visitor.MVisitor
import com.express.domain.model.visitor.MVisitorResponse
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.*
import com.rz.qrary.util.state.*

class AdminViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _scanNpmState = MutableLiveData<FormState>()
    val scanNpmState: LiveData<FormState>
        get() = _scanNpmState

    private val _visitorResponse = MutableLiveData<ResponseObject<MVisitorResponse>>()
    val visitorResponse: LiveData<ResponseObject<MVisitorResponse>>
        get() = _visitorResponse

    private val _peminjamResponse = MutableLiveData<ResponseObject<MTempPinjamResponse>>()
    val peminjamResponse: LiveData<ResponseObject<MTempPinjamResponse>>
        get() = _peminjamResponse

    private val _pengembalianResponse = MutableLiveData<ResponseArray<MHistoryDetail>>()
    val pengembalianResponse: LiveData<ResponseArray<MHistoryDetail>>
        get() = _pengembalianResponse

    private val _searchResponse = MutableLiveData<ResponseArray<MBook>>()
    val searchResponse: LiveData<ResponseArray<MBook>>
        get() = _searchResponse

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    fun checkValidityScan(type: Int, npm: String){
        _uiState.value = Loading
        if(npm.substring(0,5) == "qrary"){
            val realNpm = npm.substring(5)
            when(type){
                SCAN_PENGEMBALIAN -> checkInPengembalian(realNpm)
                SCAN_PEMINJAM -> checkInPeminjam(realNpm)
                SCAN_VISITOR -> checkInVisitor(realNpm)
            }
        } else {
            _scanNpmState.value = Invalid
            _uiState.value = Success
        }
    }

    private fun checkInPengembalian(npm: String) {
        addToDisposable(
            repository
                .getSpesificPeminjaman("npm", npm)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _pengembalianResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    private fun checkInVisitor(npm: String){
        addToDisposable(
            repository
                .addVisitor(
                    MVisitor(
                        npm,
                        getCurrentDateIn3999()
                    )
                )
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    { response ->
                        _visitorResponse.postValue(response)
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    private fun checkInPeminjam(npm: String){
        addToDisposable(
            repository
                .getTicketPeminjam(
                    MTempPinjam(
                        npm
                    )
                )
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _peminjamResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun checkIsbn(isbn: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getListBook("isbn", isbn)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _searchResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}