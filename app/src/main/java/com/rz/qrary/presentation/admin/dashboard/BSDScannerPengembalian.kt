package com.rz.qrary.presentation.admin.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.express.domain.model.account.MUser
import com.rz.qrary.util.ext.getSmallProgressDrawable
import com.rz.qrary.util.ext.loadImage
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bsd_pengembalian.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class BSDScannerPengembalian(
    private val user: MUser,
    private val ticketId: String,
    private val callback: ReturnBookCallback
) : BottomSheetDialogFragment(){

    companion object{
        @JvmStatic
        fun newInstance(
            user: MUser,
            ticketId: String,
            callback: ReturnBookCallback
        ) = BSDScannerPengembalian(user, ticketId, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bsd_pengembalian, container, false)
        view.tvBSDPengembalianName.text = user.nama
        view.tvBSDPengembalianNPM.text = user.npm
        view.ivPengembalian.loadImage(user.urlFoto, getSmallProgressDrawable(requireContext()))
        view.btnScanPengembalian.onClick {
            dismiss()
            callback.startReturnBookActivity(ticketId)
        }
        view.tvBSDReturnBookKeluar.onClick {
            dismiss()
        }
        return view
    }

    interface ReturnBookCallback{
        fun startReturnBookActivity(ticketId: String)
    }
}