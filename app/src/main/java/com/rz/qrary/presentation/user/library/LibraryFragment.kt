package com.rz.qrary.presentation.user.library

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager

import com.rz.qrary.R
import com.rz.qrary.util.base.AdapterOnClick
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.book.MBook
import com.rz.qrary.databinding.FragmentLibraryBinding
import com.rz.qrary.presentation.bookdetail.BookDetailActivity
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.other.CustomRvMargin
import com.express.domain.qrary.REQUEST_DELETE_BOOK
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

@AndroidEntryPoint
class LibraryFragment : BaseBindingFragment<FragmentLibraryBinding>(){
    private val viewModel: LibraryViewModel by viewModels()
    @Inject lateinit var rvAdapter: LibraryRvAdapter

    override fun contentView(): Int = R.layout.fragment_library

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        initRv()
        setViewListener()
        observeViewModel()
        viewModel.getLibrary()
    }

    private fun setViewListener() {
        binding.srLibrary.onRefresh {
            viewModel.getLibrary()
        }
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.libraryResponse, ::onResponse)
    }

    private fun onResponse(response: ResponseArray<MBook>) {
        rvAdapter.clearAndNotify()
        rvAdapter.insertAndNotify(response.data)
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.srLibrary.isRefreshing = false
    }

    private fun showLoadingState() {
        binding.srLibrary.isRefreshing = true
    }

    private fun initRv() {
        with(binding.rvLibrary){
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = rvAdapter
                .apply {
                    setOnItemClickListener(
                        object : AdapterOnClick {
                            override fun onRecyclerItemClicked(extra: String) {
                                navigateToDetail(extra)
                            }
                        }
                    )
                }
            addItemDecoration(
                CustomRvMargin(
                    requireContext(),
                    16,
                    CustomRvMargin.GRID_2
                )
            )
        }
    }

    private fun navigateToDetail(bookId: String) {
        startActivityForResult(
            Intent(requireContext(), BookDetailActivity::class.java)
                .apply { putExtra("bookId", bookId) },
            REQUEST_DELETE_BOOK
        )
    }
}
