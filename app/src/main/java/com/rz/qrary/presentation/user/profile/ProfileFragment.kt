package com.rz.qrary.presentation.user.profile

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.viewModels
import com.rz.qrary.R
import com.express.domain.base.ResponseArray
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.express.domain.model.account.MUser
import com.rz.qrary.databinding.FragmentProfileBinding
import com.rz.qrary.util.ext.observe
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.rz.qrary.presentation.LogoutCallback
import com.rz.qrary.presentation.user.booklist.userhistory.UserHistoryActivity
import com.rz.qrary.presentation.user.changepassword.ChangePasswordActivity
import com.rz.qrary.util.ext.gone
import com.rz.qrary.util.ext.start
import com.rz.qrary.util.ext.visible
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.toast

@AndroidEntryPoint
class ProfileFragment : BaseBindingFragment<FragmentProfileBinding>() {
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var logoutCallback: LogoutCallback

    override fun contentView(): Int = R.layout.fragment_profile

    override fun onAttach(context: Context) {
        logoutCallback = activity as LogoutCallback
        super.onAttach(context)
    }

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {
        setupViewListener()
        observeViewModel()
        viewModel.getUserData()
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.userProfileResponse, ::onResponse)
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> displayLoadingState()
            is Success -> hideLoadingState()
            is Error -> displayErrorState(state.error)
        }
    }

    private fun onResponse(response: ResponseArray<MUser>) {
        if(response.total!! > 0){
            response.data?.let{
                binding.user = it[0]
            }
        }
    }

    private fun displayErrorState(error: Throwable) {
        hideLoadingState()
        toast("${error.message}")
    }

    private fun hideLoadingState() {
        binding.srProfile.isRefreshing = false
        binding.llProfile.visible()
    }

    private fun displayLoadingState() {
        binding.llProfile.gone()
        binding.srProfile.isRefreshing = true
    }

    private fun setupViewListener() {
        binding.btnLogoutUser.onClick{
            logoutCallback.showBSDConfirmationLogout()
        }
        binding.srProfile.onRefresh {
            viewModel.getUserData()
        }
        binding.btnUbahSandi.onClick {
            gotoChangePassword()
        }
        binding.btnHubungiPengembang.onClick {
            gotoMailQrary()
        }
        binding.btnHistoryBook.onClick {
            start<UserHistoryActivity>{}
        }
    }

    private fun gotoMailQrary() {
        startActivity(
            Intent.createChooser(
                Intent(Intent.ACTION_SENDTO).apply {
                    data = Uri.parse("mailto:qrary@himatif.org")
                },
                "Kirim email"
            )
        )
    }

    private fun gotoChangePassword() {
        start<ChangePasswordActivity>{}
    }
}
