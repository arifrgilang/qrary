package com.rz.qrary.presentation.user.changepassword

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.model.account.MChangePwd
import com.express.domain.model.account.MUser
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.AppUtils
import com.express.domain.qrary.NPM
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*
import com.orhanobut.hawk.Hawk

class ChangePasswordViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _changeResponse = MutableLiveData<ResponseObject<MUser>>()
    val changeResponse: LiveData<ResponseObject<MUser>>
        get() = _changeResponse

    fun validateForm(
        oldPwd: String,
        newPwd: String,
        newPwd2: String
    ){
        _uiState.value = Loading
        if(
            oldPwd.isNotEmpty() &&
            newPwd.isNotEmpty() &&
            newPwd2.isNotEmpty() &&
            (newPwd == newPwd2) &&
            oldPwd != newPwd &&
            AppUtils.isPasswordValid(newPwd)
        ){
            _formState.value = Valid
        } else {
            _formState.value = Invalid
            _uiState.value = Success
        }
    }

    fun changePassword(
        oldPwd: String,
        newPwd: String
    ){
        _uiState.value = Loading
        addToDisposable(
            repository
                .changePasswordUser(
                    Hawk.get(NPM),
                    MChangePwd(
                        oldPwd,
                        newPwd,
                        newPwd
                    )
                )
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _changeResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}