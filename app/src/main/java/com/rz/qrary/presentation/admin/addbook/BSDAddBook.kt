package com.rz.qrary.presentation.admin.addbook

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.ext.getProgressDrawable
import com.rz.qrary.util.ext.loadImage
import com.rz.qrary.presentation.ScannerCallback
import com.express.domain.qrary.SCAN_ADD_BUKU
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bsd_add_book.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class BSDAddBook(
    private val urlFoto: String,
    private val ctx: Context,
    private val callback: ScannerCallback
) : BottomSheetDialogFragment(){
    companion object {
        @JvmStatic
        fun newInstance(
            urlFoto: String,
            ctx: Context,
            callback: ScannerCallback
        ) = BSDAddBook(urlFoto, ctx, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bsd_add_book, container, false)
        view.ivAddedBook.loadImage(
            urlFoto,
            getProgressDrawable(requireContext())
        )
        view.btnAddBookAgain.onClick {
            dismiss()
            callback.startScanner(SCAN_ADD_BUKU)
        }
        view.tvBSDAddbookKeluar.onClick {
            dismiss()
        }
        return view
    }
}