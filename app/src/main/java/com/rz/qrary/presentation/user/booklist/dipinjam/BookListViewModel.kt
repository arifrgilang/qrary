package com.rz.qrary.presentation.user.booklist.dipinjam

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.express.domain.qrary.NPM
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.model.loan.MBukuDipinjam
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.*
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState
import com.orhanobut.hawk.Hawk
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class BookListViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _listResponse = MutableLiveData<List<MBukuDipinjam>>()
    val listResponse: LiveData<List<MBukuDipinjam>>
        get() = _listResponse

    fun getDipinjamBooks(){
        val npm = Hawk.get<String>(NPM)
        _uiState.value = Loading
        addToDisposable(
            repository
                .getSpesificPeminjaman("npm", npm)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _listResponse.value = getCleanedData(it)
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    private fun getCleanedData(response: ResponseArray<MHistoryDetail>): List<MBukuDipinjam>{
        var result = mutableListOf<MBukuDipinjam>()
        // iterate all books inside list of History
        response.data?.let{
            if(it.isNotEmpty()){
                for(history in it){
                    var tanggalMeminjam =
                        getFormattedDate(history.tanggalMeminjam!!)
                    val tanggalKembali =
                        getFormattedDate(history.tanggalKembali!!)
                    for(book in history.books!!){
                        history.let{
                            result.add(
                                MBukuDipinjam(
                                    book,
                                    tanggalMeminjam,
                                    tanggalKembali,
                                    getStatus(it.tanggalKembali!!)
                                )
                            )
                        }
                    }
                }
            }
        }
        return result
    }

    // if minus = sedang meminjam, else = terlambat
    private fun getStatus(kembaliDate: String): String {
        val todayDate =  SimpleDateFormat("MM/dd/yyyy")
            .parse(getCurrentDateStandard())!!
        val kembaliDate = SimpleDateFormat("MM/dd/yyyy")
            .parse(
                getFormattedDateStandard(
                    kembaliDate
                )
            )!!
        val diff=  TimeUnit.DAYS
            .convert((todayDate.time - kembaliDate.time), TimeUnit.MILLISECONDS)
        return if(diff<=0) "Sedang Meminjam" else "Terlambat"
    }
}