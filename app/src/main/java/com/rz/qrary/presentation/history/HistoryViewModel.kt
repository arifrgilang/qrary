package com.rz.qrary.presentation.history

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.model.loan.MHistoryDetail
import com.express.domain.qrary.Repository
import com.express.domain.qrary.LIST_BORROWER
import com.express.domain.qrary.LIST_HISTORY
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState

class HistoryViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _historyResponse = MutableLiveData<ResponseArray<MHistoryDetail>>()
    val historyResponse: LiveData<ResponseArray<MHistoryDetail>>
        get() = _historyResponse

    fun getHistory(historyType: String, ticketId: String){
        when(historyType){
            LIST_BORROWER -> getHistoryBefore(ticketId)
            LIST_HISTORY -> getHistoryAfter(ticketId)
        }
    }

    private fun getHistoryBefore(ticketId: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getSpesificPeminjaman("_id", ticketId)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _historyResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    private fun getHistoryAfter(ticketId: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getSpesificHistoryPeminjaman("_id", ticketId)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _historyResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}