package com.rz.qrary.presentation.admin.visitor

import android.content.Context
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.express.domain.model.visitor.MVisitorResponse
import com.rz.qrary.databinding.ItemPengunjungBinding
import com.rz.qrary.util.other.getFormattedDate
import com.rz.qrary.util.other.getFormattedTime
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class VisitorRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<MVisitorResponse, ItemPengunjungBinding, VisitorRvAdapter.ViewHolder>(
    context
){
    override fun getResLayout(type: Int): Int = R.layout.item_pengunjung

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemPengunjungBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: MVisitorResponse) {
            model.let{
                view.user = it.user
                view.date = getFormattedDate(it.waktuMasuk!!)
                view.time = getFormattedTime(it.waktuMasuk!!)
            }
        }
    }
}