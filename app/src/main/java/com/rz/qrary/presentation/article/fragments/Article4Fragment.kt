package com.rz.qrary.presentation.article.fragments

import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.rz.qrary.databinding.FragmentArticle4Binding

class Article4Fragment : BaseBindingFragment<FragmentArticle4Binding>() {

    companion object {
        @JvmStatic
        fun newInstance() = Article4Fragment()
    }

    override fun contentView(): Int = R.layout.fragment_article4

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {}
}
