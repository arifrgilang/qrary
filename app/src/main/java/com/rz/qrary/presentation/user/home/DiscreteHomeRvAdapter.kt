package com.rz.qrary.presentation.user.home

import android.content.Context
import android.view.ViewGroup
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseRecyclerAdapter
import com.rz.qrary.databinding.ItemDiscreteBinding
import com.rz.qrary.util.ext.getSmallProgressDrawable
import com.rz.qrary.util.ext.loadImage
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class DiscreteHomeRvAdapter @Inject constructor(
    @ActivityContext context: Context?
) : BaseRecyclerAdapter<BannerModel, ItemDiscreteBinding, DiscreteHomeRvAdapter.ViewHolder>(
    context
){
    override fun getResLayout(type: Int): Int = R.layout.item_discrete

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(initViewBinding(viewType, parent))

    inner class ViewHolder(
        view: ItemDiscreteBinding
    ) : BaseViewHolder(view){
        override fun onBind(model: BannerModel) {
            view.tvDiscreteHeader.text = model.header
            view.ivDiscrete.loadImage(model.urlFoto, getSmallProgressDrawable(context!!))
        }
    }
}