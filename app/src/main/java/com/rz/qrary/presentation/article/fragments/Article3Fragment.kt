package com.rz.qrary.presentation.article.fragments

import android.os.Bundle
import com.rz.qrary.R
import com.rz.qrary.util.base.view.BaseBindingFragment
import com.rz.qrary.databinding.FragmentArticle3Binding

class Article3Fragment : BaseBindingFragment<FragmentArticle3Binding>() {

    companion object {
        @JvmStatic
        fun newInstance() = Article3Fragment()
    }

    override fun contentView(): Int = R.layout.fragment_article3

    override fun setupData(savedInstanceState: Bundle?) {}

    override fun setupView() {}
}
