package com.rz.qrary.presentation.bookdetail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseObject
import com.express.domain.model.book.MDeleteBook
import com.express.domain.qrary.Repository
import com.express.domain.model.book.MBook
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.Error
import com.rz.qrary.util.state.Loading
import com.rz.qrary.util.state.Success
import com.rz.qrary.util.state.UiState

class BookDetailViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _bookResponse = MutableLiveData<ResponseObject<MBook>>()
    val bookResponse: LiveData<ResponseObject<MBook>>
        get() = _bookResponse

    private val _deleteResponse = MutableLiveData<ResponseObject<MDeleteBook>>()
    val deleteResponse: LiveData<ResponseObject<MDeleteBook>>
        get() = _deleteResponse

    fun getBookDetail(id: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .getBook("_id", id)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _bookResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun deleteBook(bookId: String) {
        _uiState.value = Loading
        addToDisposable(
            repository
                .deleteBook(bookId)
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _deleteResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}