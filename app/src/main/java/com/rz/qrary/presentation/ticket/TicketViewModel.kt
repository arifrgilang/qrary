package com.rz.qrary.presentation.ticket

import android.annotation.SuppressLint
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.base.ResponseArray
import com.express.domain.base.ResponseObject
import com.express.domain.qrary.Repository
import com.express.domain.model.book.MBook
import com.express.domain.model.loan.MPeminjam
import com.express.domain.model.ticket.MAddBookTicket
import com.express.domain.model.ticket.MPeminjaman
import com.express.domain.model.ticket.MTempDelete
import com.express.domain.model.ticket.MTicketDetails
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class TicketViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _rvResponse = MutableLiveData<ResponseArray<MTicketDetails>>()
    val rvResponse: LiveData<ResponseArray<MTicketDetails>>
        get() = _rvResponse

    private val _savePeminjamResponse = MutableLiveData<ResponseObject<MPeminjam>>()
    val savePeminjamResponse: LiveData<ResponseObject<MPeminjam>>
        get() = _savePeminjamResponse

    private val _rvUiState = MutableLiveData<UiState>()
    val rvUiState: LiveData<UiState>
        get() = _rvUiState

    private val _uiResponse = MutableLiveData<ResponseObject<MBook>>()
    val uiResponse: LiveData<ResponseObject<MBook>>
        get() = _uiResponse

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _dateState = MutableLiveData<FormState>()
    val dateState: LiveData<FormState>
        get() = _dateState

    private val _deleteBookResponse = MutableLiveData<ResponseArray<MBook>>()
    val deleteBookResponse: LiveData<ResponseArray<MBook>>
        get() = _deleteBookResponse

    fun getListBook(ticketId: String){
        _rvUiState.value = Loading
        addToDisposable(
            repository
                .getTicketDetails(ticketId)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _rvResponse.value = it
                        _rvUiState.value = Success
                    },{
                        _rvUiState.value = Error(it)
                    }
                )
        )
    }

    fun addBookToTicket(book: MAddBookTicket){
        _uiState.value = Loading
        addToDisposable(
            repository
                .addBookToTicket(book)
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _uiResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }

    fun checkRequirements(bookCount: Int){
        _formState.value = if(bookCount > 0) Valid else Invalid
    }

    fun savePeminjaman(ticketModel: MPeminjaman){
        if(isDateValid(ticketModel.tglMeminjam!!, ticketModel.tglKembali)){
            _dateState.value = Valid
            _uiState.value = Loading
            addToDisposable(
                repository
                    .savePeminjaman(ticketModel)
                    .compose( RxUtils.applyApiCall() )
                    .subscribe(
                        {
                            _savePeminjamResponse.value = it
                            _uiState.value = Success
                        },{
                            _uiState.value = Error(it)
                        }
                    )
            )
        } else {
            _dateState.value = Invalid
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun isDateValid(dateFrom: String, dateTo: String): Boolean{
        val from = SimpleDateFormat("MM/dd/yyyy").parse(dateFrom)!!
        val to = SimpleDateFormat("MM/dd/yyyy").parse(dateTo)!!
        val diff = TimeUnit.DAYS.convert((to.time - from.time), TimeUnit.MILLISECONDS)
        return diff in 1..14
    }

    fun deleteBookFromModel(isbn: String, idTicket: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .deleteBookFromTicket(
                    MTempDelete(isbn, idTicket)
                )
                .compose( RxUtils.applyApiCall() )
                .subscribe(
                    {
                        _deleteBookResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}