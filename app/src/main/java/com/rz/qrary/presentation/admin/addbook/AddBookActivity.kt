package com.rz.qrary.presentation.admin.addbook

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.express.domain.base.ResponseObject
import com.express.domain.model.book.MAddBook
import com.express.domain.model.book.MBook
import com.express.domain.qrary.GALLERY_REQUEST_CODE
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rz.qrary.R
import com.rz.qrary.databinding.ActivityAddBookBinding
import com.rz.qrary.util.base.view.BaseBindingActivity
import com.rz.qrary.util.ext.*
import com.rz.qrary.util.state.*
import com.rz.qrary.util.view.BSDConfirmation
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AddBookActivity : BaseBindingActivity<ActivityAddBookBinding>() {
    private val viewModel: AddBookViewModel by viewModels()
    private var isbn = ""
    private lateinit var imageBitmap: Bitmap
    private var isImageInserted = false
    private var bookId = ""

    override fun contentView(): Int = R.layout.activity_add_book

    override fun setupData(savedInstanceState: Bundle?) {
        intent?.let{
            isbn = it.getStringExtra("isbn")!!
        }
        binding.tvIsbn.text = "ISBN $isbn"
    }

    override fun setupView() {
        setupToolbar("TAMBAH BUKU", true)
        setupViewListener()
        observeViewModel()
    }

    private fun observeViewModel() {
        observe(viewModel.uiState, ::handleLoading)
        observe(viewModel.formState, ::handleForm)
        observe(viewModel.addBookResponse, ::onResponse)
        observe(viewModel.updateCoverResponse, ::onUpdateCover)
    }

    private fun onUpdateCover(response: ResponseObject<MBook>) {
        if(response.total!=0){
            // Set result
            setResult(
                Activity.RESULT_OK,
                Intent().apply {
                    putExtra("urlFotoAdded", response.data?.urlCover)
                }
            )
            finish()
        } else {
            finish()
        }
    }

    private fun onResponse(response: ResponseObject<MBook>) {
        response.success.let{
            if(it){
//                bookId = response.data?.id!!
//                uploadPicture()
                setResult(
                    Activity.RESULT_OK,
                    Intent().apply {
                        putExtra("urlFotoAdded", response.data?.urlCover)
                    }
                )
                finish()
            }
        }
    }

    private fun handleForm(state: FormState) {
        when(state){
            is Invalid -> handleInvalidForm()
            is Valid -> handleValidForm()
        }
    }

    private fun handleValidForm() {
        val totalHal = if(
            binding.etTotalHalaman.text
                .toString()
                .isNotEmpty()
        ) binding.etTotalHalaman.text
                .toString()
                .toInt()
        else 0

        // Multipart
        val bahasa: RequestBody = createPartFromString(binding.etBahasa.text.toString())
        val isbnText = createPartFromString(isbn)
        val jmlHal = createPartFromString(totalHal.toString())
        val judul = createPartFromString(binding.etJudulBuku.text.toString())
        val deskripsi = createPartFromString(binding.etDeskripsiBuku.text.toString())
        val penerbit = createPartFromString(binding.etPenerbit.text.toString())
        val penulis = createPartFromString(binding.etPenulis.text.toString())
        val kategori = createPartFromString(binding.etKategoriBuku.text.toString())
        val penerjemah = createPartFromString(binding.etPenerjemah.text.toString())
        val tanggalTerbit = createPartFromString(binding.etTTTerbit.text.toString())
        val status = createPartFromString("Tersedia")

        val map = HashMap<String, RequestBody>()
        map["bahasa"] = bahasa
        map["isbn"] = isbnText
        map["jmlHal"] = jmlHal
        map["judul"] = judul
        map["deskripsi"] = deskripsi
        map["penerbit"] = penerbit
        map["penulis"] = penulis
        map["kategori"] = kategori
        map["penerjemah"] = penerjemah
        map["tanggalTerbit"] = tanggalTerbit
        map["status"] = status

        viewModel.addBook(
            map,
            createMultipart(this@AddBookActivity, imageBitmap)
        )
    }

    private fun createPartFromString(str: String) : RequestBody {
        return RequestBody.create(MultipartBody.FORM, str)
    }

    private fun handleInvalidForm() {
        toast("Lengkapi form unggah foto!")
    }

    private fun handleLoading(state: UiState) {
        when(state){
            is Loading -> showLoadingState()
            is Success -> hideLoadingState()
            is Error -> showErrorState(state.error)
        }
    }

    private fun showErrorState(error: Throwable) {
        hideLoadingState()
        toast("$error.message")
    }

    private fun hideLoadingState() {
        binding.pbAddBook.gone()
        binding.btnAddBook.visible()
    }

    private fun showLoadingState() {
        binding.btnAddBook.gone()
        binding.pbAddBook.visible()
    }

    @SuppressLint("SimpleDateFormat")
    private fun setupViewListener() {
        binding.btnAddBook.onClick {
            viewModel.validateForm(
                binding.etJudulBuku.text.toString(),
                binding.etPenulis.text.toString(),
                binding.etPenerbit.text.toString(),
                binding.etBahasa.text.toString(),
                binding.etDeskripsiBuku.text.toString(),
                binding.etKategoriBuku.text.toString(),
                isImageInserted
            )
        }
        binding.etTTTerbit.onClick {
            SpinnerDatePickerDialogBuilder()
                .context(this@AddBookActivity)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showDaySpinner(true)
                .defaultDate(2017, 0, 1)
                .callback { _, year, monthOfYear, dayOfMonth ->
                    binding.etTTTerbit.setText(
                        SimpleDateFormat("dd/MM/yyyy")
                            .format(Date(year - 1900, monthOfYear, dayOfMonth))
                    )
                }
                .build()
                .show()
        }
        binding.btnAddCover.onClick {
            checkReadWritePermision()
        }
    }

    private fun showQuitConfirmation() {
        BSDConfirmation.Builder
            .urlFoto("")
            .headerMessage("Anda yakin ingin keluar?")
            .yesButtonMessage("Ya")
            .noButtonMessage("Batal")
            .callback(
                object : BSDConfirmation.OnConfirmationCallback {
                    override fun onUserAgreed() {
                        finish()
                    }
                }
            )
            .build()
            .show(supportFragmentManager, "BSDConfirmation")
    }

    private fun checkReadWritePermision() {
        Dexter
            .withActivity(this)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(
                object : PermissionListener {
                    override fun onPermissionGranted(
                        response: PermissionGrantedResponse?
                    ) {
                        takeGalleryIntent()
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(
                        response: PermissionDeniedResponse?
                    ) {
                        toast("Permission Denied")
                        finish()
                    }
                }
            )
            .check()
    }

    private fun takeGalleryIntent() {
        startActivityForResult(
            Intent(MediaStore.ACTION_IMAGE_CAPTURE),
            GALLERY_REQUEST_CODE
        )
    }

    override fun onBackPressed() {
        showQuitConfirmation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode to resultCode) {
            GALLERY_REQUEST_CODE to Activity.RESULT_OK -> {
                imageBitmap = data?.extras?.get("data") as Bitmap
                Glide.with(this@AddBookActivity)
                    .load(imageBitmap)
                    .into(binding.ivDetailCover)

                Glide.with(this@AddBookActivity)
                    .load(imageBitmap)
                    .into(binding.ivDetailBgCover)

                isImageInserted = true
            }
        }
    }
}
