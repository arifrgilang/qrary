package com.rz.qrary.presentation.forgotpwd

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.express.domain.base.ResponseObject
import com.rz.qrary.util.base.view.BaseRxViewModel
import com.express.domain.model.auth.MEmail
import com.express.domain.qrary.Repository
import com.rz.qrary.util.other.AppUtils
import com.rz.qrary.util.other.RxUtils
import com.rz.qrary.util.state.*

class ForgotPwdViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseRxViewModel(){
    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

    private val _formState = MutableLiveData<FormState>()
    val formState: LiveData<FormState>
        get() = _formState

    private val _forgotResponse = MutableLiveData<ResponseObject<MEmail>>()
    val forgotResponse: LiveData<ResponseObject<MEmail>>
        get() = _forgotResponse

    fun validateForm(email: String){
        _uiState.value = Loading
        if(email.isNotEmpty() && AppUtils.isEmailValid(email)){
            _formState.value = Valid
        } else {
            _formState.value = Invalid
            _uiState.value = Success
        }
    }

    fun requestSendEmail(email: String){
        _uiState.value = Loading
        addToDisposable(
            repository
                .requestSendEmail(MEmail(email))
                .compose(RxUtils.applyApiCall())
                .subscribe(
                    {
                        _forgotResponse.value = it
                        _uiState.value = Success
                    },{
                        _uiState.value = Error(it)
                    }
                )
        )
    }
}